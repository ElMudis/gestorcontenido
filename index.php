<html xmlns="http://www.w3.org/1999/html">
<meta charset="utf-8">
<link rel="stylesheet" href="css/jquery-ui.min.css">
<link rel="stylesheet" href="css/jquery-ui.theme.min.css">
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/tooltipster.css">
<link rel="stylesheet" href="css/bootstrap-colorpicker.min.css">
<link rel="stylesheet" href="css/agenda.css">
<link rel="stylesheet" href="css/alertify.min.css">
<link rel="stylesheet" href="css/alertify-bootstrap.min.css">

<body>
<?php
    $lang="en";
    $uid="default";
    if(isset($_GET['lang'])){
        $lang = $_GET['lang'];
    }
    if(isset($_GET['uid'])){
        $uid = $_GET['uid'];
    }
    ?>

    <div id="designer" uid="<?php echo $uid?>">
    </div>

<div id="divPrev">

</div>

<div id="selectbox" style="display:none; opacity:0.2; z-index:10; background-color: cornflowerblue; width: 20px; height: 20px; top: 20px; left: 20px; position: absolute; border: 1px solid blue"></div>

<div id="elemPosX" hidden>-1</div>
<div id="elemPosY" hidden>-1</div>
<div id="elemAncho" hidden>-1</div>
<div id="elemAlto" hidden>-1</div>
<div id="PopupMenuCrearPizarra" class="dropdown" style="z-index: 100000; top: 60px">
    <ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu">
        <li style="z-index: 1000;"><a href="#" id="AddNota1">{{ "Nota" | trans }}</a></li>
        <li><a id="AddImagen1">{{ "Imagen" | trans }}</a></li>
        <li><a id="AddVideo1">{{ "Video" | trans }}</a></li>
        <li><a id="AddAudio1">{{ "Audio" | trans }}</a></li>
        <li><a id="AddLista1">{{ "Lista" | trans }}</a></li>
        <li><a id="AddChkLista1">{{ "Lista de opciones" | trans }}</a></li>
    </ul>
</div>

<script src="js/<?php echo $lang?>.js"></script>
<script src="js/jquery-1.11.js"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/bootstrap-colorpicker.min.js"></script>
<script src="js/jquery.top-droppable.js"></script>
<script src="js/alertify.min.js"></script>
<script src="js/jquery.json.min.js"></script>
<script src="js/nicEdit.js"></script>
<script src="js/agendaModals.js"></script>
<script src="js/agendaEvents.js"></script>
<script src="js/agenda.js"></script>
<script src="js/upload.js"></script>
<script src="js/shortcut.js"></script>



</body>
</html>
