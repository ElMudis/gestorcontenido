// idioma inglés

// [buttons]
var _btnAddContainer = "Contenedor";
var _btnAddTitle = "Título";
var _btnAddParagraph = "Párrafo";
var _btnAddRectangle = "Rectángulo";
var _btnAddLineH = "Línea Horizontal";
var _btnAddLineV = "Línea Vertical";
var _btnAddTriangle = "Triángulo";
var _btnAddCircle = "Círculo";
var _btnAddMedia = "Multimedia";
var _btnAddImage = "Imagen";
var _btnAddVideo = "Video";
var _btnAddText = "Texto";
var _btnAddShapes = "Formas";
var _btnAddTemplate = "Plantillas";
var _btnEditComponent = "Editar";
var _btnRemoveComponent = "Eliminar Componente";
var _btnPlayCode = "Correr Codigo";
var _btnSaveCode = "Salvar Codigo";
var _btnOpenCode = "Abrir Codigo";
var _btnRemove = "Remove";
var _msgDeleteElemet = "¿Está seguro que desea eliminar este elemento?";
var _msgSelectElemet = "Debe seleccionar un elemento";
var _btnAccept = "Aceptar";
var _btnCancel = "Cancelar";
var _btnSelect = "Seleccionar";
var _btnUpload = "Subir";
var _labelImage = "Imagen";
var _labelVideo = "Video";
var _labelTitle = "Título";
var _labelVideoErrorLoad = "Extensión de archivo no válida";
var _confirmDialog = "Confirmar"
var _msgErrorRemoveFile = "Ha ocurrido un error al eliminar el fichero"
var _msgErrorUploadFile = "Ha ocurrido un error al subir el fichero"
var _msgErrorInvalidFormat = "Formato no válido"
var _msgFileRemoved = "El archivo ha sido eliminado"
var _msgFileUploaded = "El archivo ha sido subido"
var _msgErrorFileMaxSize = "El archivo supera el tamaño máximo"
var _msgServerError = "Hubo un error en el servidor"
var _msgSelectAnImage = "Debe seleccionar una imagen";
var _msgSelectAVideo = "Debe seleccionar un video";
var _msgSelectAnImageFile = "Seleccione un archivo de imagen";
var _msgSelectAVideoFile = "Seleccione un archivo de video";
var _lbSelectATemplate = "Seleccione una Plantilla";
var _lbNoVideosUploaded = "No ha subido videos";
var _lbNoImagesUploaded = "No ha subido imágenes";
var _lbConfigureLine = "Configurar Línea";
var _lbConfigureText = "Configurar Texto";
var _lbConfigureRectangle = "Configurar Rectángulo";
var _lbConfigureCircle = "Configurar Círculo";
var _lbWidthPx = "Ancho (px)"
var _lbBorderWidthPx = "Ancho del borde (px)"
var _lbBorderRadiusPx = "Radio del borde (px)"
var _lbBgColor = "Color de fondo"
var _lbBorderColor = "Color del borde"
var _lbColor = "Color"
var _MaxFileSize = "Tamaño máximo"
var _lbInsertTittle = "Inserte un Título"
var _lbInsertText = "Inserte un texto aquí"
var _lbNoImageFileSelected = "Ningún archivo de imagen seleccionado"
var _lbNoVideoFileSelected = "Ningún archivo de video seleccionado"
var _lbChoose = "Buscar..."
var _btnBackground = "Cambiar Fondo de la Presentación"
var _lbBackgroundImage = "Imagen de fondo"
var _lbEditBackground = "Editar Fondo"
var _lbNoTemplates = "No hay Plantillas Disponibles"
var _lbBringToFront = "Traer al Frente"
var _lbSendToBack = "Llevar Atrás"
var _msgErrorLoadingTemplates = "Error al cargar las plantillas"

/**
 * Traducciones del NicEditor
 */
var _nicBold = "Negrita"
var _nicItalic = "Cursiva"
var _nicUnderline = "Subrayado"
var _nicLeftAlign = "Alinear texto a la izquierda"
var _nicRightAlign = "Alinear texto a la derecha"
var _nicCenterAlign = "Centrar"
var _nicJustifyAlign = "Justificar"
var _nicInsertOL = "Insertar Lista Ordenada"
var _nicInsertUL = "Insertar lista desordenada"
var _nicStrikeThrough = "Tachado"
var _nicRemoveFormatting = "Quitar formato"
var _nicIndentText = "Identar texto"
var _nicRemoveIndent = "Quitar identación"
var _nicTextColor = "Cambiar color del texto"
var _nicBackgroundColor = "Cambiar Color de Fondo"
var _nicRemoveLink = "Quitar Enlace"
var _nicFontSize = "Seleccionar Tamaño de Fuente"
var _nicFontFamily = "Seleccionar Fuente"
var _nicFontSizeCombo = "Tamaño..."
var _nicFontFamilyCombo = "Fuente..."

function format( string, arg )
{
    for ( var i = 0; i < arg.length; i++ )
    {
        string = string.replace('{' + i + '}',arg[i] );
    }
    
    return string;
}


