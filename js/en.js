// idioma inglés

// [buttons]
var _btnAddContainer = "Container";
var _btnAddTitle = "Title";
var _btnAddParagraph = "Paragraph";
var _btnAddRectangle = "Rectangle";
var _btnAddLineH = "Horizontal Line";
var _btnAddLineV = "Vertical Line";
var _btnAddTriangle = "Triangle";
var _btnAddCircle = "Circle";
var _btnAddMedia = "Media"; 
var _btnAddImage = "Image";
var _btnAddVideo = "Video";
var _btnAddText = "Text";
var _btnAddShapes = "Shapes";
var _btnAddTemplate = "Templates";
var _btnEditComponent = "Edit Component"; 
var _btnRemoveComponent = "Remove Component";
var _btnPlayCode = "Play Code";
var _btnSaveCode = "Save Code";
var _btnOpenCode = "Open Code";
var _btnRemove = "Remove";
var _msgDeleteElemet = "Are you sure you want to delete this element?";
var _msgSelectElemet = "You must select a component";
var _btnAccept = "Accept";
var _btnCancel = "Cancel";
var _btnSelect = "Select";
var _btnUpload = "Upload";
var _labelImage = "Image";
var _labelVideo = "Video";
var _labelTitle = "Title";
var _labelVideoErrorLoad = "Bad file extension";
var _confirmDialog = "Confirm"
var _msgErrorRemoveFile = "Error deleting file"
var _msgErrorUploadFile = "Error uploading file"
var _msgErrorInvalidFormat = "Invalid format"
var _msgFileRemoved = "The file has been removed"
var _msgFileUploaded = "The file has been uploaded"
var _msgErrorFileMaxSize = "The file exceeds the maximum size"
var _msgServerError = "There was a server error"
var _msgSelectAnImage = "You must select an image";
var _msgSelectAVideo = "You must select a video";
var _msgSelectAnImageFile = "Select an image file";
var _msgSelectAVideoFile = "Select a video file";
var _lbSelectATemplate = "Select a Template";
var _lbNoVideosUploaded = "No videos uploaded";
var _lbNoImagesUploaded = "No images uploaded";
var _lbConfigureLine = "Configure Line";
var _lbConfigureText = "Configure Text";
var _lbConfigureRectangle = "Configure Rectangle";
var _lbConfigureCircle = "Configure Circle";
var _lbWidthPx = "Width (px)"
var _lbBorderWidthPx = "Border width (px)"
var _lbBorderRadiusPx = "Border radius (px)"
var _lbBgColor = "Background color"
var _lbBorderColor = "Border color"
var _lbColor = "Color"
var _MaxFileSize = "Max size"
var _lbInsertTittle = "Insert a Title Here"
var _lbInsertText = "Insert a text here"
var _lbNoImageFileSelected = "No image file selected"
var _lbNoVideoFileSelected = "No video file selected"
var _lbChoose = "Choose..."
var _btnBackground = "Change Slide Background"
var _lbBackgroundImage = "Background image"
var _lbEditBackground = "Edit Background"
var _lbNoTemplates = "No Templates Available"
var _lbBringToFront = "Bring to Front"
var _lbSendToBack = "Send to Back"
var _msgErrorLoadingTemplates = "Error loading templates"

/**
 * Traducciones del NicEditor
 */
var _nicBold = "Bold"
var _nicItalic = "Italic"
var _nicUnderline = "Underline"
var _nicLeftAlign = "Left Align"
var _nicRightAlign = "Right Align"
var _nicCenterAlign = "Center Align"
var _nicJustifyAlign = "Justify Align"
var _nicInsertOL = "Justify Align"
var _nicInsertUL = "Insert Unordered List"
var _nicStrikeThrough = "Strike Through"
var _nicRemoveFormatting = "Remove Formatting"
var _nicIndentText = "Indent Text"
var _nicRemoveIndent = "Remove Indent"
var _nicTextColor = "Change Text Color"
var _nicBackgroundColor = "Change Background Color"
var _nicRemoveLink = "Remove Link"
var _nicFontSize = "Select Font Size"
var _nicFontFamily = "Select Font Family"
var _nicFontSizeCombo = "Font&nbsp;Size..."
var _nicFontFamilyCombo = "Font&nbsp;Family..."



function format( string, arg )
{
    for ( var i = 0; i < arg.length; i++ )
    {
        string = string.replace('{' + i + '}',arg[i] );
    }
    
    return string;
}


