var templates;

function initEvents() {
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
    /*Agregar una nueva nota a la pizarra actual al dar click sobre el boton #AddContainer*/
    $('#AddContainer').click(function (e) {
        AgregarNuevoContendor(
            TYPES.CONTAINER,
            $('#elemPosX').text(),
            $('#elemPosY').text(),
            $('#elemAncho').text(),
            $('#elemAlto').text(),
            "cuerpo_pizarra"
        ).focus();
    });

    //Agregado por Janier para agregar un titulo, aqui se usa el mismo metodo de agregar notas.
    $('#AddTitle').click(function (e) {
        var posx = 0;
        var posy = 0;
        var ancho = "300";
        var alto = "70";
        addNota(TYPES.TITLE, ancho, alto, posx, posy, 1, undefined, undefined, "cuerpo_pizarra");
    });

    /*Agregar una nueva nota a la pizarra actual al dar click sobre el boton #AddContainer*/
    $('#AddNota').click(function (e) {
        var posx = 0;
        var posy = 0;
        var ancho = "300";
        var alto = "200";
        addNota(TYPES.TEXT, ancho, alto, posx, posy, 1, undefined, undefined, "cuerpo_pizarra");
    });

    //Agregado por Karel para agregar un rectangulo.
    $('#AddRectangle').click(function (e) {
        var posx = 0;
        var posy = 0;
        var ancho = "200";
        var alto = "100";
        AgregarFigura(TYPES.RECTANGLE, ancho, alto, posx, posy, 1, undefined, "cuerpo_pizarra");
    });

    //Agregado por Janier para agregar un circulo.
    $('#AddCircle').click(function (e) {
        var posx = 0;
        var posy = 0;
        var ancho = "200";
        var alto = "100";
        AgregarFigura(TYPES.CIRCLE, ancho, alto, posx, posy, 1, undefined, "cuerpo_pizarra");
    });

    $('#AddLineH').click(function (e) {
        var posx = 20;
        var posy = 20;
        var ancho = "200";
        var alto = "2";

        AgregarFigura(TYPES.LINEH, ancho, alto, posx, posy, 1, undefined, "cuerpo_pizarra");
    });

    $('#AddLineV').click(function (e) {
        var posx = 20;
        var posy = 20;
        var ancho = "2";
        var alto = "200";

        AgregarFigura(TYPES.LINEV, ancho, alto, posx, posy, 1, undefined, "cuerpo_pizarra");
    });

    $('#AddTriangle').click(function (e) {
        var posx = 0;
        var posy = 0;
        var ancho = "500";
        var alto = "500";

        AgregarFigura(TYPES.TRIANGLE, ancho, alto, posx, posy, 1, undefined, "cuerpo_pizarra");
    });

    /*Abrir el modalFile para cargar una imagen y subirlo*/
    $('#AddImagen').click(function (e) {
        $('#modalMode').val('add');
        $('#bgPizarra').hide();
        $('#bgImage').hide();
        $('#title-File').html(_btnSelect + " " + _labelImage);

        $('#tipoMedia').val("Imagen");// tipo de media que se va cargar (imagen)
        $('#modalType').val("Media");// tipo de ventana que se va a levantar

        mostrarImagenes(); //Cargar las Imagenes del servidor

        $('#myFileDiv').html('<input id="myFile" type="file" name="myfile" class="filestyle">');
        $('input#myFile').attr('accept', 'image/*');
        $(":file").filestyle({
            buttonName: "btn-info",
            buttonText: _lbChoose,
            placeholder: _lbNoImageFileSelected,
            buttonBefore: true,
            size: "sm",
            iconName: "glyphicon glyphicon-picture"
        });
    });

    /*Abrir el modalFile para cargar un video y subirlo como elemento a la pizarra actual*/
    $('#AddVideo').click(function (e) {
        $('#modalMode').val('add');
        $('#bgPizarra').hide();
        $('#bgImage').hide();

        $('#tipoMedia').val("Video"); // tipo de media que se va cargar (video)
        $('#modalType').val("Media")
        mostrarVideos(); //Cargar los videos del servidor

        $('#title-File').html(_btnSelect + " " + _labelVideo);

        $('#myFileDiv').html('<input id="myFile" type="file" name="myfile" class="filestyle">');
        //solo admitir los formatos permitidos para reproducir en el navegador
        $('input#myFile').attr('accept', '.mp4, .ogg, .webm');
        $(":file").filestyle({
            buttonName: "btn-info",
            buttonText: _lbChoose,
            placeholder: _lbNoVideoFileSelected,
            buttonBefore: true,
            size: "sm",
            iconName: " glyphicon glyphicon-facetime-video"
        });
    });

    $('#editBackground').on('click', function () {
        $('#bgPizarra').show();
        $('#bgImage').show();
        $('#tipoMedia').val("Imagen");// tipo de media que se va cargar (imagen)
        $('#modalType').val("Background");
        $('#title-File').html(_lbEditBackground);

        //Ver callback "always"
        mostrarImagenes();

    });

    $('#addTemplate').on('click', function () {
        mostrarPlantillas();
    });

    $('#modalWidgetNotaAceptar').click(function (e) {

        var json = $.parseJSON(widgetActualizado.attr("propiedades"));
        json['border-width'] = $("#anchoBordeTexto").val() + "px";
        json['border-radius'] = $("#radioBordeTexto").val() + "px";

        json['background'] = $('#colorFondoTexto').colorpicker('getValue');
        json['border-color'] = $('#colorBordeTexto').colorpicker('getValue');

        establecerStyle(
            widgetActualizado,
            json['padding'],
            json['width'],
            json['height'],
            json['overflow'],
            json['border-width'],
            json['border-style'],
            json['border-radius'],
            json['border-color'],
            json['font-size'],
            json['opacity'],
            json['zIndex'],
            json['background'],
            json['top'],
            json['left'],
            json['position']
        );
    });

    $('#modalWidgetSquareAceptar').click(function (e) {

        var json = $.parseJSON(widgetActualizado.attr("propiedades"));
        json['border-width'] = $("#anchoBordeCuadrado").val() + "px";
        json['border-radius'] = $("#radioBordeCuadrado").val() + "px";

        json['background'] = $('#colorFondoCuadrado').colorpicker('getValue');
        json['border-color'] = $('#colorBordeCuadrado').colorpicker('getValue');

        establecerStyle(
            widgetActualizado,
            json['padding'],
            json['width'],
            json['height'],
            json['overflow'],
            json['border-width'],
            json['border-style'],
            json['border-radius'],
            json['border-color'],
            json['font-size'],
            json['opacity'],
            json['zIndex'],
            json['background'],
            json['top'],
            json['left'],
            json['position']
        );
    });

    $('#modalWidgetCircleAceptar').click(function (e) {

        var json = $.parseJSON(widgetActualizado.attr("propiedades"));
        json['border-width'] = $("#anchoBordeCirculo").val() + "px";

        json['background'] = $('#colorFondoCirculo').colorpicker('getValue');
        json['border-color'] = $('#colorBordeCirculo').colorpicker('getValue');

        establecerStyle(
            widgetActualizado,
            json['padding'],
            json['width'],
            json['height'],
            json['overflow'],
            json['border-width'],
            json['border-style'],
            json['border-radius'],
            json['border-color'],
            json['font-size'],
            json['opacity'],
            json['zIndex'],
            json['background'],
            json['top'],
            json['left'],
            json['position']
        );
    });

    $('#modalWidgetLineHAceptar').click(function (e) {

        var json = $.parseJSON(widgetActualizado.attr("propiedades"));
        json['height'] = $("#anchoLineaH").val() + "px";
        json['background'] = $('#colorLineaH').colorpicker('getValue');

        establecerStyle(
            widgetActualizado,
            json['padding'],
            json['width'],
            json['height'],
            json['overflow'],
            json['border-width'],
            json['border-style'],
            json['border-radius'],
            json['border-color'],
            json['font-size'],
            json['opacity'],
            json['zIndex'],
            json['background'],
            json['top'],
            json['left'],
            json['position']
        );
    });

    $('#modalWidgetLineVAceptar').click(function (e) {

        var json = $.parseJSON(widgetActualizado.attr("propiedades"));
        json['width'] = $("#anchoLineaV").val() + "px";
        json['background'] = $('#colorLineaV').colorpicker('getValue');

        establecerStyle(
            widgetActualizado,
            json['padding'],
            json['width'],
            json['height'],
            json['overflow'],
            json['border-width'],
            json['border-style'],
            json['border-radius'],
            json['border-color'],
            json['font-size'],
            json['opacity'],
            json['zIndex'],
            json['background'],
            json['top'],
            json['left'],
            json['position']
        );
    });

    $('#editComponent').on('click', function () {
        editComponent();
    });

    $('#removeComponent').on('click', function () {
        removeComponent();
    });

    function destroyClickedElement(event)
    {
        document.body.removeChild(event.target);
    }

    $('#openCode').on('click', function () {
    });

    $('#saveCode').on('click', function () {
        var textToSave = $('#cuerpo_editar').html();
        var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
        var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
        var fileNameToSaveAs = "xczxb.html";

        var downloadLink = document.createElement("a");
        downloadLink.download = fileNameToSaveAs;
        downloadLink.innerHTML = "Download File";
        downloadLink.href = textToSaveAsURL;
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);

        downloadLink.click();
    });

    $('#playCode').on('click', function () {
        var textToSave = playCode();
        var textToSaveAsBlob = new Blob([textToSave], {type:"text/plain"});
        var textToSaveAsURL = window.URL.createObjectURL(textToSaveAsBlob);
        var fileNameToSaveAs = "xczxb.html";

        var downloadLink = document.createElement("a");
        downloadLink.download = fileNameToSaveAs;
        downloadLink.innerHTML = "Download File";
        downloadLink.href = textToSaveAsURL;
        downloadLink.onclick = destroyClickedElement;
        downloadLink.style.display = "none";
        document.body.appendChild(downloadLink);

        downloadLink.click();

    });

    $('input[name="optBackground"]').on('change', function (e) {
        var bgType = $('input[name="optBackground"]:checked').val();
        if (bgType == 'color') {
            $('input[name="optradio"]').prop("disabled", true);
            $(":file").filestyle('disabled', true);
            $("#boton_subir").prop("disabled", true);
            $("#boton_eliminar").prop("disabled", true);
            $("#boton_seleccionar").prop("disabled", false);
            $("#bgColorPizarra").colorpicker('enable');
        } else if (bgType == 'image') {
            $('input[type="radio"]').prop("disabled", false);
            $(":file").filestyle('disabled', false);
            $("#boton_subir").prop("disabled", false);
            $("#boton_seleccionar").prop("disabled", false);
            $("#bgColorPizarra").colorpicker('disable');
            if ($('[name="optradio"]:checked').val()) {
                $("#boton_eliminar").prop("disabled", false);
                $("#boton_seleccionar").prop("disabled", false);
            } else {
                $("#boton_eliminar").prop("disabled", true);
                $("#boton_seleccionar").prop("disabled", true);
            }
        }
    });

    $('#boton_seleccionar_plantilla').on('click', function () {
        var id_selected = $('label.active').attr('value');
        $('#cuerpo_editar').html(templates[id_selected].body);
        componentSelected = null;
        $('#modalTemplates').modal("hide");
        Editar();
    });

    $('#bringFront').on('click', function(){
        if(componentSelected == null){
            alertify.error(_msgSelectElemet);
        }else{
            traerAlFrente(componentSelected);
        }
    });

    $('#sendBack').on('click', function(){
        if(componentSelected == null){
            alertify.error(_msgSelectElemet);
        }else{
            llevarAlFondo(componentSelected);
        }
    });

}

/* Metodo auxiliar para controlar el comportamiento a un elemento(padre)
 y sus componentes tras varios eventos
 */
function EventosWidget(tipo, padre, DivCuerpo, DivTitulo, cantI, logo) {

    padre.resizable({
            stop: function (event, ui) {
                if (cantI > 0) {
                    for (var j = 0; j < DivCuerpo.length; j++) {
                        DivCuerpo[j].css({"width": (padre.width() - 30)});
                        $("#" + DivCuerpo[j].attr("id") + " .itemLista").css({"width": (padre.width() - 30)});
                    }
                    padre.css({"height": (DivCuerpo.length * 30) + 35 + DivTitulo.height()});
                    DivTitulo.css({"width": padre.width() - 75});
                }
                else {
                    if (tipo === "Nota") {
                        // alert("dd");
                        if (logo != "") {
                            DivTitulo.css({"width": padre.width() - 65});
                        }
                        else {
                            DivTitulo.css({"width": padre.width() - 25});
                        }
                        DivCuerpo.css({"width": padre.width() - 10});

                        padre.css({"height": padre.height() + 9, "width": padre.width() + 13});
                    }
                }
            }
        });
    padre.draggable({
        // drag: function (event, ui) {
        //     hacerPadresVisible(padre);
        // },
        stop: function (event, ui) {
            // deshacerPadresVisible(padre);
            var mov = false;
            $('.cPiz.toCopy').each(function (index, n) {
                mov = true;
                $(padre).hide();
                $(this).removeClass('toCopy');
                var data = {idPizarra: $(n).children().attr("id"), idElem: $(padre).attr("idElem"), tipo: tipo};
                $(padre).remove();
                // }, function(fail) {
                $(padre).show();
            });
            // });
            if (!mov) {
                if ($("#navbar").height() + 1 > $(padre).position().top) {
                    $(padre).css("top", ($("#navbar").height() + 1) + "px");
                }
            }
        }
    });
    if (tipo === "Nota") {
        padre.topDroppable({
            drop: function (e, ui) {
                var texto = $(this).children(".itemLista")[1];
                $(texto).append($($(ui.helper.context).children()[1]).clone());//..ht("<div>fffff:ffdd<div>");

                $(DivCuerpo).blur();
            }
        }).droppable({
            accept: '.alert-info'
        });
    }
}

function addCommonWidgetEvents(widget) {
    widget.resizable({
        alsoResize: ".element-selected",
        start: function (event, ui) {
            if (!$(this).hasClass("element-selected")) {  // Loop through each element and store beginning start and left positions
                $(".element-selected").each(function (i) {
                    $(this).removeClass("element-selected");
                    console.info('7');
                });
            }
        },
        stop: function (event, ui) {
            var propiedades = $.evalJSON(widget.attr('propiedades'));
            propiedades.width = widget.width;
            propiedades.height = widget.height;
            var a = $.toJSON(propiedades);
            widget.attr('propiedades', a);
        }
    });

    widget.draggable({
        start: function (event, ui) {
            posTopArray = [];
            posLeftArray = [];
            if ($(this).hasClass("element-selected")) {  // Loop through each element and store beginning start and left positions
                $(".element-selected").each(function (i) {
                    thiscsstop = $(this).css('top');
                    if (thiscsstop == 'auto') thiscsstop = 0; // For IE

                    thiscssleft = $(this).css('left');
                    if (thiscssleft == 'auto') thiscssleft = 0; // For IE

                    posTopArray[i] = parseInt(thiscsstop);
                    posLeftArray[i] = parseInt(thiscssleft);
                });
            }else{
                $(".element-selected").each(function (i) {
                    $(this).removeClass("element-selected");
                    console.info('8');
                });
            }

            begintop = $(this).offset().top; // Dragged element top position
            beginleft = $(this).offset().left; // Dragged element left position
        },
        drag: function (event, ui) {
            var topdiff = $(this).offset().top - begintop;  // Current distance dragged element has traveled vertically
            var leftdiff = $(this).offset().left - beginleft; // Current distance dragged element has traveled horizontally

            if ($(this).hasClass("element-selected")) {
                $(".element-selected").each(function (i) {
                    $(this).css('top', posTopArray[i] + topdiff); // Move element veritically - current css top + distance dragged element has travelled vertically
                    $(this).css('left', posLeftArray[i] + leftdiff); // Move element horizontally - current css left + distance dragged element has travelled horizontally
                });
            }
        }
    });
}

function mostrarPlantillas() {
    $.ajax({
        // url: location.origin + '/?q=admin/opigno/content/slide_templates/design',
        url: 'http://localhost/gestorcontenido/upload_files/templates.php',
        dataType: 'JSON',
        type: 'POST',
        success: function (respuesta) {
            innerHTML = '';
            if (respuesta.length > 0) {
                templates = respuesta;
                var container = '<div id="template_container" class="btn-group-vertical col-xs-12" data-toggle="buttons"></div>';
                $("#plantillas").html(container);
                for (var i = 0; i < respuesta.length; i++) {
                    if (respuesta[i] != undefined) {
                        var label = '<label id="template_' + respuesta[i].id + '" class="btn btn-default' + ( i == 0 ? ' active ' : '' ) + '" value= ' + i + ' style="text-align: left"></label>';
                        $("#template_container").append(label);
                        var htmlAux =
                            '<input type="radio" name="template" ><h4>' + respuesta[i].title + '</h4> ' +
                            '<p style="white-space: normal; word-wrap: break-word"> ' + respuesta[i].description + '</p>';
                        $('#template_' + respuesta[i].id).html( htmlAux );
                    }
                }
                $('#boton_seleccionar_plantilla').prop("disabled", false);
            } else {
                $('#boton_seleccionar_plantilla').prop("disabled", true);
                innerHTML =
                    '<div class="block"> ' +
                    '<div class="inner-block">' +
                    '<div class="text-center">' +
                    '<span class="glyphicon glyphicon-file" style="text-align: center; color:#565656; font-size: 6em"> </span>' +
                    '</div>' +
                    '<div class="text-center">' +
                    '<h2 style="font-size: 30px; color: #373737; font-family: inherit"> ' + _lbNoTemplates + '</h2>' +
                    '</div>' +
                    '</div>' +
                    '</div> '
            }

            $('#modalTemplates').modal({show: true, keyboard: false, backdrop: 'static'});// Mostrar dialogo

        },
        error: function (response) {
            // alertify.error(_msgErrorLoadingTemplates);
            $('#boton_seleccionar_plantilla').prop("disabled", true);
            innerHTML =
                '<div class="block"> ' +
                '<div class="inner-block">' +
                '<div class="text-center">' +
                '<span class="glyphicon glyphicon-file" style="text-align: center; color:#565656; font-size: 6em"> </span>' +
                '</div>' +
                '<div class="text-center">' +
                '<h2 style="font-size: 30px; color: #373737; font-family: inherit"> ' + _lbNoTemplates + '</h2>' +
                '</div>' +
                '</div>' +
                '</div> '
            $('#modalTemplates').modal({show: true, keyboard: false, backdrop: 'static'});// Mostrar dialogo
        }
    });
}

