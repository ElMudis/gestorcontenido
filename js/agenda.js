var timeout;
var myNicEditor;

//variable agregada por Janier para controlar que componente está seleccionado.
var componentSelected;
// ID del usuario logeado en el LMS.
var uid;
//Controla el componente que esta mas al frente
var topIndex = 1;

//Controla el componente que esta mas al fondo
var backIndex = 1;

/*Metodo jQuery que se ejecuta luego que la pagina ha sido cargada completamente donde se cargan
 todos los datos correspondientes a la pizarra activa y otras configuraciones generales */
$(document).ready(function () {
    uid = $('#designer').attr('uid');
    createModals(); //Funcion definida en agendaModals.js
    $(".colores").colorpicker();
    myNicEditor = new nicEditor({buttonList: ['fontFamily', 'fontSize', 'bold', 'italic', 'underline', 'center', 'left', 'right', 'justify', 'indent', 'outdent', 'ul', 'ol', 'forecolor', 'bgcolor', 'removeformat', 'unlink']});
    myNicEditor.setPanel('myNicPanel');
    initEvents(); ////Funcion definida en agendaEvents.js
    cuerpoPizarraEvents();


});

//*******Added by Janier*******//
//este objeto lo puse para saber en cada memento con que tipo de componente estoy trabajando
//ademas puede ser util para agregarle metadatos a los componentes.
var TYPES = {
    CONTAINER: {
        idCmp: 1,
        string: 'Caja'
    },
    TEXT: {
        idCmp: 2,
        string: 'Campo de texto'
    },
    TITLE: {
        idCmp: 3,
        string: 'T&iacute;tulo'
    },
    LINEH: {
        idCmp: 4,
        string: 'L&iacute;nea Horizontal'
    },
    RECTANGLE: {
        idCmp: 5,
        string: 'Cuadrilatero'
    },
    IMAGE: {
        idCmp: 6,
        string: 'Campo Imagen'
    },
    TABLE: {
        idCmp: 7,
        string: 'Tabla'
    },
    LINEV: {
        idCmp: 8,
        string: 'L&iacute;nea Vertical'
    },
    CIRCLE: {
        idCmp: 9,
        string: 'Ciacute;rculo'
    },
    VIDEO: {
        idCmp: 10,
        string: 'Video'
    },
    TRIANGLE: {
        idCmp: 11,
        string: 'Triacute;ngulo'
    }
};

function getType(widget) {
    var type = widget.attr("idcmptype");
    switch (type) {
        case "1":
            return TYPES.CONTAINER;
        case "2":
            return TYPES.TEXT;
        case "3":
            return TYPES.TITLE;
        case "4":
            return TYPES.LINEH;
        case "5":
            return TYPES.RECTANGLE;
        case "6":
            return TYPES.IMAGE;
        case "8":
            return TYPES.LINEV;
        case "9":
            return TYPES.CIRCLE;
        case "10":
            return TYPES.VIDEO;
        case "11":
            return TYPES.TRIANGLE;
    }
}

function desabilitarDraggable(widget) {
    $(widget).draggable('disable');
    if (widget.parent().attr("id") !== "cuerpo_pizarra") {
        desabilitarDraggable(widget.parent());
    }
}

function habilitarDraggable(widget) {
    $(widget).draggable('enable');
    if (widget.parent().attr("id") !== "cuerpo_pizarra") {
        habilitarDraggable(widget.parent());
    }
}

/*Agregar un nuevo contenedor al editor*/
function AgregarNuevoContendor(cmptype, pPosx, pPosy, pAncho, pAlto, contenedor, strJson) {

    // Creacion de todos los datos de un contenedor
    var posx = pPosx;
    var posy = pPosy;
    var ancho = pAncho;
    var alto = pAlto;

    if (pPosx === "-1" || pPosx === "") {
        posx = Math.floor(Math.random() * 300) + 50;
        posy = Math.floor(Math.random() * 300) + 50;
        ancho = "350";
        alto = "200";
    }

    var idNumero = Math.floor(Math.random() * 10000);
    var contenedor = AgregarContendor(cmptype, "d_c_" + idNumero, ancho, alto, posx, posy, 1, idNumero, idNumero, strJson, contenedor);

    if (strJson != undefined && strJson != "") {
        var json = $.parseJSON(strJson);
        json['top'] = posx;
        json['left'] = posy;
        establecerStyle(
            contenedor,
            json['padding'],
            json['width'],
            json['height'],
            json['overflow'],
            json['border-width'],
            json['border-style'],
            json['border-radius'],
            json['border-color'],
            json['font-size'],
            json['opacity'],
            json['zIndex'],
            json['background'],
            json['top'],
            json['left'],
            json['position'],
            json['orientation']
        );
    }
    return contenedor;
}

function AgregarContendor(cmptype, id, ancho, alto, posx, posy, zIndex, idElem, idWidget, strJson, container) {
    var div = '	<div id="' + id + '" class="contene lms" > </div>';
    $("#" + container).append(div); // Se le adicionan al cuerpo de la pizarra el div
    var contenedor = $("#" + id);

    // se le agrega unos atributos nesesarios a esta lista.
    contenedor.attr('idElem', idElem);
    contenedor.attr('idwidget', idWidget);
    contenedor.attr({"tabindex": "-1"}); //esto permite que se marque el elemento cuando obtenga el focus
    contenedor.attr({"idcmptype": cmptype.idCmp});
    contenedor.cmpType = TYPES.CONTAINER;

    // establecer los estilos css
    var hasStyle = (strJson !== undefined && strJson !== "");
    var styles;
    if (hasStyle) {
        styles = $.toJSON(strJson);
        styles.top = posx + 'px';
        styles.left = posy + 'px';
    }
    else {
        styles = {
            'width': ancho + 'px',
            'height': alto + 'px',
            'top': posx + 'px',
            'left': posy + 'px',
            'opacity': '1',
            'z-index': zIndex,
            'position': 'absolute',
            'padding': '3px',
            'overflow': 'hidden',
            'border-width': '1px',
            'border-style': 'solid',
            'border-radius': '0px',
            'border-color': 'black'
        };
    }
    establishStyle(contenedor, styles);
    contenedorEvents(contenedor);

    return contenedor;
}

/////// NOTAS
function addNota(cmptype, ancho, alto, posx, posy, zIndex, strJson, html, contenedor) {
    var number = Math.floor(Math.random() * 10000);
    var id = "d_n_" + number;
    var div;
    switch (cmptype) {
        case TYPES.TEXT:
            div = '	<div id="' + id + '" class="dragg notas paragraphnota lms" > ' + _lbInsertText + '</div>';
            break;
        case TYPES.TITLE:
            div = '	<div id="' + id + '" align = "center" class="dragg notas titlenota lms" >' + _lbInsertTittle + '</div>';
            break;
    }

    // Se le adicionan al cuerpo de la pizarra el div
    $("#" + contenedor).append(div);
    var nota = $("#" + id);
    //var editor = myNicEditor.addInstance(id);

    // se le agrega unos atributos nesesarios a esta lista.
    nota.attr('idElem', number);
    nota.attr('idwidget', number);
    nota.attr({"idcmptype": cmptype.idCmp});
    nota.attr({"tabindex": "-1"});

    var hasStyle = (strJson !== undefined && strJson !== "");
    var styles;
    if (hasStyle) {
        styles = $.evalJSON(strJson);
        styles.top = posy + 'px';
        styles.left = posx + 'px';

    }
    else {
        styles = {
            'width': ancho + 'px',
            'height': alto + 'px',
            'top': posx + 'px',
            'left': posy + 'px',
            'opacity': '1',
            'z-index': topIndex += 1,
            'position': 'absolute',
            'orientation': 'horizontal',
            'word-wrap': 'break-word'
        };
    }

    // estableciendo los estilos css y los modals para cada tipo de componente.
    switch (cmptype) {
        case TYPES.TEXT:
            nota.cmpType = TYPES.TEXT;
            if (!hasStyle) {
                var textStyle = {
                    'font-size': '12',
                    'padding': '1px',
                    'overflow': 'hidden',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-radius': '0px',
                    'border-color': '#000000',
                    'background': '#ffffff'
                };
                styles = $.extend(true, styles, textStyle);
            }
            break;
        case TYPES.TITLE:
            nota.cmpType = TYPES.TITLE;
            if (!hasStyle) {
                var titleStyle = {
                    'font-size': '30',
                    'padding': '1px',
                    'overflow': 'hidden',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-radius': '0px',
                    'border-color': '#000000',
                    'background': '#ffffff'
                };
                styles = $.extend(true, styles, titleStyle);
            }
            break;
    }
    establishStyle(nota, styles);

    if (html != undefined && html != "") {
        //    nota.resizable("destroy"); quitado provisionalmente
        nota.html(html);
        nota.children(".ui-resizable-handle").remove();
    }
    notaEvents(nota);
    return nota;
}

function AgregarFigura(cmptype, ancho, alto, posx, posy, zIndex, strJson, contenedor) {

    // Una lista es un elemento div que se le activas unas propiedades jquery.ui dragable y rezisable
    var number = Math.floor(Math.random() * 10000);
    var id = "d_f_" + number;
    var div;
    switch (cmptype) {
        case TYPES.LINEH:
            div = '	<div id="' + id + '" class="dragg figura linea lineah lms" > </div>';
            break;
        case TYPES.LINEV:
            div = '	<div id="' + id + '" class="dragg figura linea lineav lms" > </div>';
            break;
        case TYPES.RECTANGLE:
            div = '	<div id="' + id + '" class="dragg figura cuadrilatero lms" ></div>';
            break;
        case TYPES.CIRCLE:
            div = '	<div id="' + id + '" class="dragg figura circulo lms" ></div>';
            break;
        case TYPES.TRIANGLE:
            div = '	<div id="' + id + '" class="dragg figura triangle-up lms" >&#8593;</div>';
            break;
    }

    // Se le adicionan al cuerpo de la pizarra el div
    $("#" + contenedor).append(div);
    var figura = $("#" + id);

    // se le agrega unos atributos nesesarios
    figura.attr('idElem', number);
    figura.attr('idwidget', number);
    figura.attr({"tabindex": "-1"}); //esto permite que se marque el elemento cuando obtenga el focus
    figura.attr({"idcmptype": cmptype.idCmp});

    var hasStyle = (strJson !== undefined && strJson !== "");
    var styles;
    if (hasStyle) {
        styles = $.evalJSON(strJson);
        styles.top = posy + 'px';
        styles.left = posx + 'px';

    }
    else {
        styles = {
            'width': ancho + 'px',
            'height': alto + 'px',
            'top': posx + 'px',
            'left': posy + 'px',
            'opacity': '1',
            'z-index': topIndex += 1,
            'position': 'absolute',
            'orientation': 'horizontal'
        };
    }

    // estableciendo los estilos css y los modals para cada tipo de componente.
    switch (cmptype) {
        case TYPES.LINEH:
            figura.cmpType = TYPES.LINEH;
            if (!hasStyle) {
                var lineStyle = {'padding': '0px', 'overflow': 'hidden', 'background': 'black'};
                styles = $.extend(true, styles, lineStyle);
            }
            break;
        case TYPES.LINEV:
            figura.cmpType = TYPES.LINEV;
            if (!hasStyle) {
                var lineStyle = {
                    'padding': '0px',
                    'overflow': 'hidden',
                    'border-radius': '0px',
                    'background': 'black'
                };
                styles = $.extend(true, styles, lineStyle);
            }
            break;
        case TYPES.RECTANGLE:
            figura.cmpType = TYPES.RECTANGLE;
            if (!hasStyle) {
                var rectangleStyle = {
                    'padding': '3px',
                    'overflow': 'hidden',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-radius': '0px',
                    'border-color': 'black',
                    'background': 'white'
                };
                styles = $.extend(true, styles, rectangleStyle);
            }
            break;
        case TYPES.CIRCLE:
            figura.cmpType = TYPES.CIRCLE;
            if (!hasStyle) {
                var circleStyle = {
                    'padding': '3px',
                    'overflow': 'hidden',
                    'border-width': '1px',
                    'border-style': 'solid',
                    'border-radius': '50%',
                    'border-color': 'black',
                    'background': 'white'
                };
                styles = $.extend(true, styles, circleStyle);
            }
            break;
        case TYPES.TRIANGLE:
            figura.cmpType = TYPES.TRIANGLE;
            if (!hasStyle) {
                var triangleStyle = {
                    'overflow': 'hidden',
                    'font-size': '500px'
                };
                styles = $.extend(true, styles, triangleStyle);
            }
            break;
    }
    establishStyle(figura, styles);
    figuraEvents(figura);
    return figura;
}

function establishStyle(widget, properties) {
    var eval = $.toJSON(properties);
    widget.attr("propiedades", eval);
    widget.css(properties);
}

function Subir(padre, cant, subir) {

    if (subir) {
        padre.css({'z-index': padre.css('z-index') - (-1000)});
    }
    else {
        if (padre.css('z-index') > 1000) {
            padre.css({'z-index': padre.css('z-index') - 1000});
        }
    }

}

function establecerStyle(widget, padding, ancho, alto, overflow, borderWidth,
                         borderStyle, borderRadius, borderColor,
                         fontSize, opacity, zIndex, background,
                         posx, posy, position) {
    var text = '  {';
    text += '"padding": "' + padding + '",';
    text += '"width": "' + ancho + '",';
    text += '"height": "' + alto + '",';
    text += '"word-wrap": "break-word",';
    text += '"overflow": "' + overflow + '",';
    text += '"border-width": "' + borderWidth + '",';
    text += '"border-style": "' + borderStyle + '",';
    text += '"border-radius": "' + borderRadius + '",'; //added by janier
    text += '"border-color": "' + borderColor + '",';
    text += '"font-size": "' + fontSize + 'px",';
    text += '"opacity": "' + opacity + '",';
    text += '"z-index": "' + zIndex + '",';
    text += '"background": "' + background + '",';
    text += '"top": "' + posy + 'px",';
    text += '"left": "' + posx + 'px",';
    text += '"position": "' + position + '"';
    text += '}';


    widget.attr("propiedades", text);
    var json = $.parseJSON(widget.attr("propiedades"));
    widget.css(json);
    widget.draggable({containment: 'document'});
//    widget.resizable({zIndex: zIndex, handles: "e, w", containment: $("#"+contenedor)});

}


function establecerStyleJson(widget, strJson) {
    var json = $.parseJSON(strJson);
    establecerStyle(
        widget,
        json['padding'],
        json['width'],
        json['height'],
        json['overflow'],
        json['border-width'],
        json['border-style'],
        json['border-radius'],
        json['border-color'],
        json['font-size'],
        json['opacity'],
        json['zIndex'],
        json['background'],
        json['top'],
        json['left'],
        json['position'],
        json['orientation']
    );
}
var widgetActualizado;
function guardarFondoWidget(widget, tipoCmp) {
    widgetActualizado = widget;
    switch (tipoCmp) {
        case TYPES.TEXT: //Campo de texto
            var json = $.parseJSON(widget.attr("propiedades"));
            var anchoBordeTexto = json['border-width'];
            var radioBordeTexto = json['border-radius'];
            $("#anchoBordeTexto").val(anchoBordeTexto.substr(0, anchoBordeTexto.length - 2));
            $("#radioBordeTexto").val(radioBordeTexto.substr(0, radioBordeTexto.length - 2));
            $('#colorFondoTexto').colorpicker('setValue', json['background']);
            $('#colorBordeTexto').colorpicker('setValue', json['border-color']);

            $('#modalWidgetNota').modal({show: true, keyboard: false, backdrop: 'static'});
            break;
        case TYPES.TITLE: //Campo de texto
            var json = $.parseJSON(widget.attr("propiedades"));
            var anchoBordeTexto = json['border-width'];
            var radioBordeTexto = json['border-radius'];
            $("#anchoBordeTexto").val(anchoBordeTexto.substr(0, anchoBordeTexto.length - 2));
            $("#radioBordeTexto").val(radioBordeTexto.substr(0, radioBordeTexto.length - 2));
            $('#colorFondoTexto').colorpicker('setValue', json['background']);
            $('#colorBordeTexto').colorpicker('setValue', json['border-color']);

            $('#modalWidgetNota').modal({show: true, keyboard: false, backdrop: 'static'});
            break;
        case TYPES.LINEH: //Linea Horizontal
            var json = $.parseJSON(widget.attr("propiedades"));
            var ancho = json['height'];
            $("#anchoLineaH").val(ancho.substr(0, ancho.length - 2));
            $('#colorLineaH').colorpicker('setValue', json['background']);

            $('#modalWidgetLineH').modal({show: true, keyboard: false, backdrop: 'static'});
            break;
        case TYPES.RECTANGLE: //Rectangulo
            var json = $.parseJSON(widget.attr("propiedades"));
            var anchoBordeCuadrado = json['border-width'];
            var radioBordeCuadrado = json['border-radius'];
            $("#anchoBordeCuadrado").val(anchoBordeCuadrado.substr(0, anchoBordeCuadrado.length - 2));
            $("#radioBordeCuadrado").val(radioBordeCuadrado.substr(0, radioBordeCuadrado.length - 2));
            $('#colorFondoCuadrado').colorpicker('setValue', json['background']);
            $('#colorBordeCuadrado').colorpicker('setValue', json['border-color']);

            $('#modalWidgetSquare').modal({show: true, keyboard: false, backdrop: 'static'});
            break;
        case TYPES.LINEV: //Linea Vertical
            var json = $.parseJSON(widget.attr("propiedades"));
            var ancho = json['width'];
            $("#anchoLineaV").val(ancho.substr(0, ancho.length - 2));
            $('#colorLineaV').colorpicker('setValue', json['background']);

            $('#modalWidgetLineV').modal({show: true, keyboard: false, backdrop: 'static'});
            break;
        case TYPES.CIRCLE:
            var json = $.parseJSON(widget.attr("propiedades"));
            var anchoBordeCirculo = json['border-width'];
            $('#anchoBordeCirculo').val(anchoBordeCirculo.substr(0, anchoBordeCirculo.length - 2));
            $('#colorFondoCirculo').colorpicker('setValue', json['background']);
            $('#colorBordeCirculo').colorpicker('setValue', json['border-color']);

            $('#modalWidgetCircle').modal({show: true, keyboard: false, backdrop: 'static'});
            break;
        case TYPES.IMAGE:
            $('#modalMode').val('edit');
            $('#bgPizarra').hide();
            $('#bgImage').hide();
            $('#title-File').html(_btnSelect + " " + _labelImage);
            $('#tipoMedia').val("Imagen");// tipo de media que se va cargar (imagen)
            $('#modalType').val("Media");// tipo de ventana que se va a levantar
            $(":file").filestyle('disabled', false);
            mostrarImagenes(); //Cargar las Imagenes del servidor
            break;
        case TYPES.VIDEO:
            $('#modalMode').val('edit');
            $('#bgPizarra').hide();
            $('#bgImage').hide();
            $('#title-File').html(_btnSelect + " " + _labelVideo);
            $('#tipoMedia').val("Video");// tipo de media que se va cargar (imagen)
            $('#modalType').val("Media");// tipo de ventana que se va a levantar
            $(":file").filestyle('disabled', false);
            mostrarVideos(); //Cargar los videos del servidor
            break;
    }
}

/*Agregar una Imagen al cuerpo de la pagina web*/
function AgregarImagen(id, archivo, ancho, alto, posx, posy, zIndex, idElem, idWidget, contenedor) {
    // Una Imagen es un elemento div que se le activas unas propiedades jquery.ui dragable y rezisable
    var div = '	<div id="' + id + '" class="medias imagen lms">  </div>';
    // Se le adicionan al cuerpo de la pizarra el div
    $("#" + contenedor).append(div);
    var imagen = $("#" + id);
    // se le agrega unos atributos nesesarios a esta chklista.
    imagen.attr('media_url', archivo);
    imagen.attr('idElem', idElem);
    imagen.attr('idwidget', idWidget);
    imagen.attr({"idcmptype": 6});

    imagen.attr({"tabindex": "-1"});    //esto permite que se marque el elemento cuando obtenga el focus
    //  se  activan las propiedades jquery.ui dragable y rezisable
    //imagen.draggable({containment: $("#" + contenedor)});
    //imagen.resizable({zIndex: zIndex, handles: "e, s, se", containment: $("#" + contenedor)});
    $(".ui-icon-gripsmall-diagonal-se").css({"background": "url('') "});
    // se configuran los css

    imagen.css({
        'opacity': 1,
        "background": "url(" + archivo + ")",
        "background-repeat": "no-repeat",
        "background-size": "100% 100%",
        "top": "0px",
        "left": "0px",
        "width": 500,
        "z-Index": topIndex += 1,
        "heigth": 500,
        "position": "absolute"
    });
    imagen.height(alto);
    imagen.width(ancho);
    imagen.cmpType = TYPES.IMAGE;

    imagenEvents(imagen);
}

/*Agregar un Video al cuerpo de la pagina web*/
function AgregarVideo(id, archivo, ancho, alto, posx, posy, zIndex, idElem, idWidget, contenedor) {
    // Se le adicionan al cuerpo de la pizarra el div
    var div = '<div id="' + id + '" class="medias video lms"> </div>';
    $("#" + contenedor).append(div);
    var vi1 = '<video id="VideoInt' + id + '" width= 100% height= 100%  class="lms asdads" controls preload="none" src="' + archivo + '" ></video>';
    $("#" + id).append(vi1);
    var nuevoVideo = $("#" + id);
    // se le agrega unos atributos nesesarios a esta chklista.
    nuevoVideo.attr('media_url', archivo);
    nuevoVideo.attr('idElem', idElem);
    nuevoVideo.attr('idwidget', idWidget);
    nuevoVideo.attr({"idcmptype": 10});
    //  se  activan las propiedades jquery.ui dragable y rezisable
    //nuevoVideo.draggable({containment: $("#" + contenedor)});
    //nuevoVideo.resizable({zIndex: zIndex + 1, handles: "e, s, se", containment: $("#" + contenedor)});
    $(".ui-icon-gripsmall-diagonal-se").css({"background": "url('') "});
    // se configuran los css
    nuevoVideo.css({
        "top": "0px",
        "left": "0px",
        "width": ancho,
        "height": alto,
        "z-Index": topIndex += 1,
        "min-width": '200px',
        "min-height": '150px',
        "background": "gray",
        "position": "absolute"
    });
    nuevoVideo.height(ancho);
    nuevoVideo.width(alto);
    nuevoVideo.cmpType = TYPES.VIDEO;
    videoEvents(nuevoVideo);
}

/*Modificar el color de fondo de un elemento
 se obtiene el color de fondo del boton que se le pasa
 y se le da un nuevo color a este boton*/

/*Modificar el texto de una Nota */
function guardarContenidoWidget(padre, hijo, cont) {
    var contenido = $(hijo).html();
}

/* Metodo auxiliar traer al frente a un elemento(padre)*/
function traerAlFrente(widget) {
    widget.css("z-index", topIndex += 1);
}

/* Metodo auxiliar traer al frente a un elemento(padre)*/
function llevarAlFondo(widget) {
    widget.css("z-index", backIndex -= 1);
}

function pocicionDeUnElemento(widget) {

    if ($(widget).attr("id") === "cuerpo_pizarra") {
        return {top: 0, left: 0};
    }

    var left = $(widget).position().left;
    var top = $(widget).position().top;

    left += pocicionDeUnElemento($(widget).parent()).left;
    top += pocicionDeUnElemento($(widget).parent()).top;
    return {top: top, left: left};
}

// function hacerPadresVisible(widget) {
//
//     if ($(widget).attr("id") === "cuerpo_pizarra") {
//         return;
//     }
//     $(widget).css("z-index", 10000);
//     $(widget).css("overflow", "hidden");
//     hacerPadresVisible($(widget).parent());
//     return;
// }
//
// function deshacerPadresVisible(widget) {
//
//     if ($(widget).attr("id") === "cuerpo_pizarra") {
//         return;
//     }
//     $(widget).css("z-index", 5);
//     $(widget).css("overflow", "hidden");
//     deshacerPadresVisible($(widget).parent());
//     return;
// }

//funciones para sustituir los botones de los componentes
function editComponent() {
    if (componentSelected) {
        switch (getType(componentSelected)) {
            case TYPES.TITLE: {
                guardarFondoWidget(componentSelected, TYPES.TITLE);
                break;
            }
            case TYPES.TEXT: {
                guardarFondoWidget(componentSelected, TYPES.TEXT);
                break;
            }
            case TYPES.LINEV: {
                guardarFondoWidget(componentSelected, TYPES.LINEV);
                break;
            }
            case TYPES.LINEH: {
                guardarFondoWidget(componentSelected, TYPES.LINEH);
                break;
            }
            case TYPES.RECTANGLE: {
                guardarFondoWidget(componentSelected, TYPES.RECTANGLE);
                break;
            }
            case TYPES.CIRCLE: {
                guardarFondoWidget(componentSelected, TYPES.CIRCLE);
                break;
            }
            case TYPES.IMAGE: {
                guardarFondoWidget(componentSelected, TYPES.IMAGE);
                break;
            }
            case TYPES.VIDEO: {
                guardarFondoWidget(componentSelected, TYPES.VIDEO);
                break;
            }
        }
    } else {
        alertify.error(_msgSelectElemet);
    }
}

function removeComponent() {

    var elementSelected = $("#cuerpo_pizarra .element-selected");

    console.info ($(elementSelected));
    console.info (elementSelected.length);
    $(elementSelected).each(function (i) {
        $(this).remove();
    });
    return false;
    if (componentSelected) {
        alertify.confirm(_confirmDialog, _msgDeleteElemet, function (e) {
                if (e) {
                    componentSelected.remove();
                    componentSelected = null;
                }
                else {
                    componentSelected.focus();
                }
            }, null
        );
    } else {
        alertify.error(_msgSelectElemet);
    }
}

function playCode() {
    $("#cuerpo_hidden").html($('#cuerpo_editar').html());
    return $('#cuerpo_hidden').html();
}

function Editar() {



    var elements = $('#cuerpo_pizarra div.lms');
    console.info(elements);
    $.each(elements, function (index, element) {
        var number = Math.floor(Math.random() * 10000);

        $(element).attr('idElem', number);
        $(element).attr('idwidget', number);

        if ($(element).hasClass('notas')) {
            $(element).attr('id', "d_f_" + number);
            notaEvents($(element));
        } else if ($(element).hasClass('figura')) {
            $(element).attr('id', "d_f_" + number);
            figuraEvents($(element));
        } else if ($(element).hasClass('imagen')) {
            $(element).attr('id', "imagen_" + number);
            imagenEvents($(element));
        } else if ($(element).hasClass('video')) {
            $(element).attr('id', "video_" + number);
            videoEvents($(element));
        } else if ($(element).hasClass('contene')) {
            $(element).attr('id', "d_c_" + number);
            contenedorEvents($(element));
        }
    });

    console.info(elements);
}

function notaEvents(nota) {
    nota.children().remove();
    console.info(nota.prop('id'));

    var editor = myNicEditor.addInstance(nota.prop('id'));
    nota.attr({"contenteditable": "false"});
    nota.resizable({zIndex: nota.css("z-index"), handles: "e, s, se", containment: nota.parent()});


    nota.on("dblclick", function (uc) {
        desabilitarDraggable(nota);
        nota.resizable('disable');
        nota.attr('contenteditable', 'true');
        nota.attr("cmpNotaEdit", "true");
        nota.blur();
        nota.css({"cursor": "auto"});
        nota.attr("cmpNotaEdit", "false");
        nota.trigger("focus");
    });

    nota.on("click", function () {
        nota.trigger("focus");
    });

    nota.focus(function (uc) {
        nota.attr({"focused": "true"});
        componentSelected = nota;
        $(".element-selected").each(function (i) {
            $(this).removeClass("element-selected");
            console.info('1');
        });
        nota.addClass("element-selected");
    });

    nota.on("blur", function (uc) {
        $('#removeComponent').attr('enable', false);
        nota.attr({"focused": "false"});

        if (nota.attr("cmpNotaEdit") === "false") {
            setTimeout(function () {
                if (!nota.hasClass("nicEdit-selected")) {
                    nota.attr('contenteditable', 'false');
                    habilitarDraggable(nota);
                    nota.resizable('enable');
                    nota.css({"cursor": "move"});
                }
            }, 150);
        }
    });

    nota.on("scroll", function (uc) {
        nota.css({"height": uc.currentTarget.scrollHeight});
    });

    nota.draggable({containment: nota.parent()});
    //$(".ui-icon-gripsmall-diagonal-se").css({"background": "url('') "});
    addCommonWidgetEvents(nota);
}

function figuraEvents(figura) {
    if (figura.cmpType == TYPES.LINEH) {
        figura.resizable({zIndex: figura.css("z-index"), handles: "e", containment: figura.parentElement});
    } else if (figura.cmpType == TYPES.LINEV) {
        figura.resizable({zIndex: figura.css("z-index"), handles: "s", containment: figura.parentElement});
    } else if (figura.cmpType == TYPES.RECTANGLE || figura.cmpType == TYPES.CIRCLE || figura.cmpType == TYPES.TRIANGLE) {
        figura.resizable({zIndex: figura.css("z-index"), handles: "e, s, se", containment: figura.parentElement});
    }
    figura.on("click", function () {
        figura.trigger("focus");
    });

    figura.focus(function (uc) {
        figura.attr({"focused": "true"});
        componentSelected = figura;
        $(".element-selected").each(function (i) {
            $(this).removeClass("element-selected");
            console.info('2');
        });
        figura.addClass("element-selected");
    });

    figura.on("blur", function (uc) {
        figura.attr({"focused": "false"});
    });

    figura.draggable({containment: $('#cuerpo_pizarra')});
    $(".ui-icon-gripsmall-diagonal-se").css({"background": "url('') "});
    addCommonWidgetEvents(figura);
}

function imagenEvents(imagen) {
    imagen.draggable({containment: $("#" + imagen[0].parentElement.id)});
    imagen.resizable({
        zIndex: imagen.css("z-index"),
        handles: "e, s, se",
        containment: $("#" + imagen[0].parentElement.id)
    });
    imagen.focus(function () {
        imagen.attr({"focused": "true"});
        componentSelected = imagen;
        $(".element-selected").each(function (i) {
            $(this).removeClass("element-selected");
            console.info('3');
        });
        imagen.addClass("element-selected");
    });
    EventosWidget("Media", imagen, null, null, 0, "");
}

function videoEvents(video) {
    video.draggable({containment: $("#" + video[0].parentElement.id)});
    video.resizable({
        zIndex: video.css("z-index") + 1,
        handles: "e, s, se",
        containment: $("#" + video[0].parentElement.id)
    });
    EventosWidget("Media", video, null, null, 0, "");
    video.children()[0].addEventListener("click",
        function () {
            $(".element-selected").each(function (i) {
                $(this).removeClass("element-selected");
                console.info('4');
            });
            video.addClass("element-selected");
            video.attr({"focused": "true"});
            componentSelected = video;
        });
}


function contenedorEvents(contenedor) {

    contenedor.children(".ui-resizable-handle").remove();
    contenedor.topDroppable(
        {
            start: function (event, ui) {

                posTopArray = [];
                posLeftArray = [];
                if ($(this).hasClass("circulo")) {  // Loop through each element and store beginning start and left positions
                    $(".circulo").each(function (i) {
                        thiscsstop = $(this).css('top');
                        if (thiscsstop == 'auto') thiscsstop = 0; // For IE

                        thiscssleft = $(this).css('left');
                        if (thiscssleft == 'auto') thiscssleft = 0; // For IE

                        posTopArray[i] = parseInt(thiscsstop);
                        posLeftArray[i] = parseInt(thiscssleft);
                    });
                }

                begintop = $(this).offset().top; // Dragged element top position
                beginleft = $(this).offset().left; // Dragged element left position
            },
            drop: function (e, ui) {

                var topdiff = $(this).offset().top - begintop;  // Current distance dragged element has traveled vertically
                var leftdiff = $(this).offset().left - beginleft; // Current distance dragged element has traveled horizontally

                if ($(this).hasClass("circulo")) {
                    $(".circulo").each(function (i) {
                        $(this).css('top', posTopArray[i] + topdiff); // Move element veritically - current css top + distance dragged element has travelled vertically
                        $(this).css('left', posLeftArray[i] + leftdiff); // Move element horizontally - current css left + distance dragged element has travelled horizontally
                    });
                }

                var X = ui.position.left - pocicionDeUnElemento($(this)).left - $(this).parent().position().left;
                var Y = ui.position.top - pocicionDeUnElemento($(this)).top - $(this).parent().position().top;

                var type = getType($(ui.draggable));

                if ($(ui.draggable).hasClass("notas")) {
                    if ($(ui.helper).parent().attr("id") !== $(this).attr("id")) {
                        var X1 = pocicionDeUnElemento($(ui.helper).parent()).left + ui.position.left;
                        var Y1 = pocicionDeUnElemento($(ui.helper).parent()).top + ui.position.top;

                        var X = X1 - pocicionDeUnElemento($(this)).left;
                        var Y = Y1 - pocicionDeUnElemento($(this)).top;

                        addNota(
                            type,
                            0, 0, X, Y,
                            1,
                            $(ui.helper).attr("propiedades"),
                            $(ui.helper).html(),
                            $(this).attr("id")
                        ).focus();
                        $(ui.helper).remove();
                    }
                }
                else if ($(ui.draggable).hasClass("figura")) {
                    if ($(ui.helper).parent().attr("id") !== $(this).attr("id")) {
                        var X1 = pocicionDeUnElemento($(ui.helper).parent()).left + ui.position.left;
                        var Y1 = pocicionDeUnElemento($(ui.helper).parent()).top + ui.position.top;

                        var X = X1 - pocicionDeUnElemento($(this)).left;
                        var Y = Y1 - pocicionDeUnElemento($(this)).top;

                        AgregarFigura(
                            type,
                            0, 0, X, Y,
                            $(ui.helper).zIndex(),
                            $(ui.helper).attr("propiedades"),
                            $(this).attr("id")
                        ).focus();
                        $(ui.helper).remove();
                    }
                }
            }
        }).droppable({
        greedy: true,
        //activeClass: "ui-state-default",
        hoverClass: "ui-state-hover",
        accept: ".dragg"

    });

    contenedor.focus(function (uc) {
        if(!$(contenedor).hasClass('element-selected')) {
            $(".element-selected").each(function (i) {
                $(this).removeClass("element-selected");
                console.info('5');
            });
            contenedor.addClass("element-selected");
            contenedor.attr({"focused": "true"});
            componentSelected = contenedor;
        }
    });

    contenedor.on("blur", function (uc) {
        contenedor.attr({"focused": "false"});
    });

    contenedor.draggable({containment: $('#cuerpo_pizarra')});
    contenedor.resizable({
        zIndex: contenedor.css("z-index"),
        handles: "e, s, se",
        containment: $("#" + contenedor[0].parentElement.id)
    });
    $(".ui-icon-gripsmall-diagonal-se").css({"background": "url('') "});
    addCommonWidgetEvents(contenedor);
}


function selectElement(X1, Y1, X2, Y2) {
    $("#cuerpo_pizarra").children().each(function () {
        var top = $(this).position().top + $("#cuerpo_editar").position().top;
        var left = $(this).position().left;
        var ftop = $(this).position().top + $(this).height() + $("#cuerpo_editar").position().top;
        var fleft = $(this).position().left + $(this).width();

        if ((X1 < left && left < X2) && (Y1 < top && top < Y2) && (X1 < fleft && fleft < X2) && (Y1 < ftop && ftop < Y2)) {
            $(this).addClass("element-selected");
        } else {
            $(this).removeClass("element-selected");
            console.info('6');
        }

    });
}


var PosInicialX = -1;
var PosInicialY = -1;
function cuerpoPizarraEvents() {



    shortcut.add("delete", function (e) {

        removeComponent();

    }, {
        'disable_in_input': true,
        'propagate': true,
    });

    shortcut.add("Ctrl+C", function (e) {
        if ($(".notas[contenteditable='true']").length == 0) {

            $("#cuerpo_hidden").html("");
            $(".element-selected").each(function () {
                $("#cuerpo_hidden").append($(this).clone());
            });
        }
    }, {
        'disable_in_input': true,
        'propagate': true,
    });
    shortcut.add("Ctrl+V", function (e) {
        if ($(".notas[contenteditable='true']").length == 0) {
            $("#cuerpo_hidden > *").each(function () {
                $("#cuerpo_pizarra").append($(this).clone());
            });

            Editar();
        }
    }, {
        'disable_in_input': true,
        'propagate': true,
    });
    shortcut.add("Ctrl+X", function (e) {
        if ($(".notas[contenteditable='true']").length == 0) {
            $("#cuerpo_hidden").html("");
            $(".element-selected").each(function () {
                $("#cuerpo_hidden").append($(this).clone());
                $(this).remove()
            });
        }
    }, {
        'disable_in_input': true,
        'propagate': true,
    });

    $('#cuerpo_pizarra').on('click', function (obj) {
        var value = $(obj.target).hasClass('lms');
        if (value === false) {
            componentSelected = null;
        }
    });


    $("#cuerpo_pizarra").mousedown(function (e) {
        $('#elemPosX').text("-1");
        $('#elemPosY').text("-1");
        $('#elemAncho').text("-1");
        $('#elemAlto').text("-1");

        $('#PopupMenuCrearPizarra').removeClass("open");

        if (e.target.id === "cuerpo_pizarra") {
            $('#cuerpo_pizarra').addClass("noselect");
            PosInicialX = e.clientX;
            PosInicialY = e.clientY;
        }
        else {
            PosInicialX = -1;
            PosInicialY = -1;
        }
//    e.preventDefault();
//    return false;
    });

    $("#cuerpo_pizarra, #selectbox").mouseup(function (e) {
        if (PosInicialX > -1) {
            var X1 = 0, X2 = 0, Y1 = 0, Y2 = 0;
            if (PosInicialX > e.clientX) {
                X1 = e.clientX;
                X2 = PosInicialX;

            } else {
                X1 = PosInicialX;
                X2 = e.clientX;
            }
            if (PosInicialY > e.clientY) {
                Y1 = e.clientY;
                Y2 = PosInicialY;

            } else {
                Y1 = PosInicialY;
                Y2 = e.clientY;
            }

            selectElement(X1, Y1, X2, Y2);

            PosInicialX = -1;
            PosInicialY = -1;
            $('#selectbox').hide();
        }
    });

    $("#cuerpo_pizarra, #selectbox").mousemove(function (e) {


        if (PosInicialX > -1) {


            $('#cuerpo_pizarra').removeClass("noselect");
            var x = -1;
            var y = -1;
            var ancho = -1;
            var alto = -1;
            if (PosInicialX > e.clientX) {
                x = e.clientX;
                ancho = PosInicialX - e.clientX;

            } else {
                x = PosInicialX;
                ancho = e.clientX - PosInicialX;
            }
            if (PosInicialY > e.clientY) {
                y = e.clientY;
                alto = PosInicialY - e.clientY;

            } else {
                y = PosInicialY;
                alto = e.clientY - PosInicialY;
            }
            if (ancho > 15 && alto > 15) {
                $('#elemPosX').text(x);
                $('#elemPosY').text(y);
                $('#elemAncho').text(ancho);
                $('#elemAlto').text(alto);
                $('#selectbox').show();
                $('#selectbox').css({"left": x + "px", "top": y + "px", "width": ancho + "px", "height": alto + "px"});

                // $('#selectbox').hide();

                $('#PopupMenuCrearPizarra').css({"top": e.clientY, "left": e.clientX});
                $('#PopupMenuCrearPizarra').addClass("open");
            }
        }
//    e.preventDefault();
//    return false;
    });
}