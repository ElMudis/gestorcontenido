//AGREGADA POR JANIER
//ESTA VARIABLE ES LA QUE MUESTA LAS IMAGENES O LOS VIDEOS.
//SE HIZO ASI PARA USAR EL MISMO MODAL.
var innerHTML = "";

function eliminarArchivo(archivo) {
    if ($('#tipoMedia').val() == "Imagen") {
        $data = {archivo: archivo, tipoMedia: "Imagen", uid: uid};
    } else if ($('#tipoMedia').val() == "Video") {
        $data = {archivo: archivo, tipoMedia: "Video", uid: uid};
    }
    $.ajax({
        url: 'upload_files/eliminar_archivo.php',
        type: 'POST',
        timeout: 10000,
        data: $data,
        error: function () {
            alertify.error(_msgErrorRemoveFile);
        },
        success: function (respuesta) {
            if (respuesta == 1) {
                alertify.success(_msgFileRemoved);
            } else {
                mostrarRespuesta(_msgErrorRemoveFile);
            }
            //mostrarImagenes();
            mostrarArchivos();
        }
    });
}

function mostrarArchivos() {
    if ($('#tipoMedia').val() == "Imagen") {
        mostrarImagenes();
    } else if ($('#tipoMedia').val() == "Video") {
        mostrarVideos();
    }
}

function mostrarImagenes() {
    $.ajax({
        url: 'upload_files/mostrar_archivos.php',
        dataType: 'JSON',
        type: 'POST',
        data: {'tipoMedia': 'imagen', 'uid': uid},
        success: function (respuesta) {
            if (respuesta[0] == true) {
                innerHTML = '';
                if (respuesta[1].length > 0) {
                    for (var i = 0; i < respuesta[1].length; i += 6) {
                        var htmlAux = "";
                        //if (respuesta[i] != undefined) {
                        if (respuesta[1][i] != undefined) {
                            var id = "img" + respuesta[1][i];
                            htmlAux += '<div style="margin: 8px" class="row"> ' +
                            '<div class="col-xs-2"> ' +
                            '<div class="radio">' +
                            '<input type="radio" name="optradio" value="' + respuesta[1][i] + '">' +
                            '<image "height="40" width="40" src="upload_files/files/' + uid + '/images/' + respuesta[1][i] + '"></image>' +
                            '</div>' +
                            '</div> '
                        }
                        if (respuesta[1][i + 1] != undefined) {
                            htmlAux += ' <div class="col-xs-2"> ' +
                            '<div class="radio">' +
                            '<input type="radio" name="optradio" value="' + respuesta[1][i + 1] + '">' +
                            '<image height="40" width="40" src="upload_files/files/' + uid + '/images/' + respuesta[1][i + 1] + '"></image>' +
                            '</div> ' +
                            '</div> '
                        }
                        if (respuesta[1][i + 2] != undefined) {
                            htmlAux += ' <div class="col-xs-2"> ' +
                            '<div class="radio">' +
                            '<input type="radio" name="optradio" value="' + respuesta[1][i + 2] + '">' +
                            '<image height="40" width="40" src="upload_files/files/' + uid + '/images/' + respuesta[1][i + 2] + '"></image>' +
                            '</div> ' +
                            '</div>'
                        }
                        if (respuesta[1][i + 3] != undefined) {
                            htmlAux += ' <div class="col-xs-2"> ' +
                            '<div class="radio">' +
                            '<input type="radio" name="optradio" value="' + respuesta[1][i + 3] + '">' +
                            '<image height="40" width="40" src="upload_files/files/' + uid + '/images/' + respuesta[1][i + 3] + '"></image>' +
                            '</div> ' +
                            '</div> '
                        }
                        if (respuesta[1][i + 4] != undefined) {
                            htmlAux += ' <div class="col-xs-2"> ' +
                            '<div class="radio">' +
                            '<input type="radio" name="optradio" value="' + respuesta[1][i + 4] + '">' +
                            '<image height="40" width="40" src="upload_files/files/' + uid + '/images/' + respuesta[1][i + 4] + '"></image>' +
                            '</div> ' +
                            '</div> '
                        }
                        if (respuesta[1][i + 5] != undefined) {
                            htmlAux += ' <div class="col-xs-2"> ' +
                            '<div class="radio">' +
                            '<input type="radio" name="optradio" value="' + respuesta[1][i + 5] + '">' +
                            '<image height="40" width="40" src="upload_files/files/' + uid + '/images/' + respuesta[1][i + 5] + '"></image>' +
                            '</div> ' +
                            '</div> '
                        }
                        if (respuesta[1][i] != undefined) {
                            htmlAux += '</div> '
                        }
                        ;
                        //}
                        innerHTML += htmlAux;
                    }
                } else {
                    innerHTML =
                        '<div class="block"> ' +
                        '<div class="inner-block">' +
                        '<div class="text-center">' +
                        '<span class="glyphicon glyphicon-picture" style="text-align: center; color:#565656; font-size: 6em"> </span>' +
                        '</div>' +
                        '<div class="text-center">' +
                        '<h2 style="font-size: 30px; color: #373737; font-family: inherit"> ' + _lbNoImagesUploaded + '</h2>' +
                        '</div>' +
                        '</div>' +
                        '</div> '
                }
                $("#archivos_subidos").html(innerHTML);
                $('#modalFile').modal({show: true, keyboard: false, backdrop: 'static'});// Mostrar dialogo
                $("#boton_eliminar").prop("disabled", true);
                //$("#boton_subir").prop("disabled", true);
                $("#boton_seleccionar").prop("disabled", true);
            } else {
                alertify.error(_msgServerError);
            }
        }
    }).always(function () {

        $('#myFileDiv').html('<input id="myFile" type="file" name="myfile" class="filestyle">');
        $('input#myFile').attr('accept', 'image/*');
        $(":file").filestyle({
            buttonName: "btn-info",
            buttonText: _lbChoose,
            placeholder: _lbNoImageFileSelected,
            buttonBefore: true,
            size: "sm",
            iconName: "glyphicon glyphicon-picture"
        });
        var backgroundType = $('#backgroundType').val();
        if ($("#modalType").val() == "Background") {
            if (backgroundType == 'color') {
                $('input[name="optBackground"][value="color"]').prop("checked", true);
                var bg = $("#cuerpo_pizarra").css('background-color');
                $("#bgColorPizarra").colorpicker('enable');
                $('#bgColorPizarra').colorpicker('setValue', bg);
                $('input[name="optradio"]').prop("disabled", true);
                $(":file").filestyle('disabled', true);
                $("#boton_subir").prop("disabled", true);
                $("#boton_seleccionar").prop("disabled", false);
                $(":file").filestyle({disabled: true});
            } else if (backgroundType == 'imagen') {
                $('input[name="optBackground"][value="image"]').prop("checked", true);
                var bg = $("#cuerpo_pizarra").css('background-image');
                $('input[type="radio"]').prop("disabled", false);
                var imagename = $("#cuerpo_pizarra").attr('imagename');
                $('input[name="optradio"][value="' + imagename + '"]').attr('checked', true);
                $(":file").filestyle('disabled', false);
                $("#boton_subir").prop("disabled", false);
                $("#boton_seleccionar").prop("disabled", false);
                $("#boton_eliminar").prop("disabled", false);
                $("#bgColorPizarra").colorpicker('disable');
            }
        }
    });
}

function mostrarVideos() {
    $.ajax({
        url: 'upload_files/mostrar_archivos.php',
        dataType: 'JSON',
        type: 'POST',
        data: {'tipoMedia': 'video', 'uid': uid},
        success: function (respuesta) {
            if (respuesta[0] == true) {
                innerHTML = '';
                var Mp4Ogg = '';
                var webm = '';
                if (respuesta[1].length > 0) {
                    for (var i = 0; i < respuesta[1].length; i += 3) {
                        var htmlAux = "";
                        //if (respuesta[i] != undefined) {
                        if (respuesta[1][i] != undefined) {

                            Mp4Ogg = respuesta[1][i].substring([respuesta[1][i].length - 4], [respuesta[1][i].length]);
                            webm = respuesta[1][i].substring([respuesta[1][i].length - 5], [respuesta[1][i].length]);

                            htmlAux += '<div style="margin: 8px" class="row"> ' +
                            '<div class="col-sm-4"> ' +
                            '<div class="radio">'
                            if (Mp4Ogg == ".mp4" || Mp4Ogg == ".ogg" || webm == ".webm") {
                                htmlAux += '<input type="radio" name="optradio" value="' + respuesta[1][i] + '">' +
                                '<video class="video-player" controls height="100" width="140" src="upload_files/files/' + uid + '/videos/' + respuesta[1][i] + '"></video><p> <strong>' + _labelTitle + ': </strong>' + respuesta[1][i] + ' </p>'
                            } else {
                                htmlAux += '<input type="radio" name="optradio" value="' + respuesta[1][i] + '">' +
                                '<div style="text-align: center; color: #C9302C"> ' + _labelVideoErrorLoad + ' <span class="glyphicon glyphicon-remove-sign" style="text-align: center; color:#C9302C; font-size: 5em; height: 80; width: 140px"> </span></div><p> <strong>' + _labelTitle + ': </strong> ' + respuesta[1][i] + ' </p>'
                            }
                            htmlAux += '</div>' +
                            '</div> '
                        }

                        if (respuesta[1][i + 1] != undefined) {

                            Mp4Ogg = respuesta[1][i + 1].substring([respuesta[1][i + 1].length - 4], [respuesta[1][i + 1].length]);
                            webm = respuesta[1][i + 1].substring([respuesta[1][i + 1].length - 5], [respuesta[1][i + 1].length]);

                            htmlAux += ' <div class="col-sm-4"> ' +
                            '<div class="radio">'
                            if (Mp4Ogg == ".mp4" || Mp4Ogg == ".ogg" || webm == ".webm") {
                                htmlAux += '<input type="radio" name="optradio" value="' + respuesta[1][i + 1] + '">' +
                                '<video class="video-player" controls height="100" width="140" src="upload_files/files/' + uid + '/videos/' + respuesta[1][i + 1] + '"></video><p> <strong>' + _labelTitle + ': </strong> ' + respuesta[1][i + 1] + ' </p>'
                            } else {
                                htmlAux += '<input type="radio" name="optradio" value="' + respuesta[1][i + 1] + '">' +
                                '<div style="text-align: center; color: #C9302C"> ' + _labelVideoErrorLoad + ' <span class="glyphicon glyphicon-remove-sign" style="text-align: center; color:#C9302C; font-size: 5em; height: 80; width: 140px"> </span></div><p> <strong>' + _labelTitle + ': </strong> ' + respuesta[1][i] + ' </p>'
                            }
                            htmlAux += '</div>' +
                            '</div> '
                        }
                        if (respuesta[1][i + 2] != undefined) {
                            Mp4Ogg = respuesta[1][i + 2].substring([respuesta[1][i + 2].length - 4], [respuesta[1][i + 2].length]);
                            webm = respuesta[1][i + 2].substring([respuesta[1][i + 2].length - 5], [respuesta[1][i + 2].length]);

                            htmlAux += ' <div class="col-sm-4"> ' +
                            '<div class="radio">'
                            if (Mp4Ogg == ".mp4" || Mp4Ogg == ".ogg" || webm == ".webm") {
                                htmlAux += '<input type="radio" name="optradio" value="' + respuesta[1][i + 2] + '">' +
                                '<video class="video-player" controls height="100" width="140" src="upload_files/files/' + uid + '/videos/' + respuesta[i + 2] + '"></video><p> <strong>' + _labelTitle + ': </strong> ' + respuesta[i + 2] + ' </p>'
                            } else {
                                htmlAux += '<input type="radio" name="optradio" value="' + respuesta[1][i + 2] + '">' +
                                '<div style="text-align: center; color: #C9302C"> ' + _labelVideoErrorLoad + ' <span class="glyphicon glyphicon-remove-sign" style="text-align: center; color:#C9302C; font-size: 5em; height: 80; width: 140px"> </span></div><p> <strong>' + _labelTitle + ': </strong> ' + respuesta[1][i] + ' </p>'
                            }
                            htmlAux += '</div>' +
                            '</div>'
                        }
                        if (respuesta[1][i] != undefined) {
                            htmlAux += '</div> '
                        }
                        ;
                        //}
                        innerHTML += htmlAux;
                    }
                } else {
                    innerHTML =
                        '<div class="block"> ' +
                        '<div class="inner-block">' +
                        '<div class="text-center">' +
                        '<span class="glyphicon glyphicon-facetime-video" style="text-align: center; color:#565656; font-size: 6em"> </span>' +
                        '</div>' +
                        '<div class="text-center">' +
                        '<h2 style="font-size: 30px; color:#373737; font-family: inherit"> ' + _lbNoVideosUploaded + '</h2>' +
                        '</div>' +
                        '</div>' +
                        '</div> '
                }
                $("#archivos_subidos").html(innerHTML);
                $('#modalFile').modal({show: true, keyboard: false, backdrop: 'static'});// Mostrar dialogo
                $("#boton_eliminar").prop("disabled", true);
                //$("#boton_subir").prop("disabled", true);
                $("#boton_seleccionar").prop("disabled", true);
            } else {
                alertify.error(_msgServerError);
            }
        }
    });
}

function seleccionarArchivos(archivo) {

    var idNumero = Math.floor(Math.random() * 10000);
//    var titulo = _tituloNota;
    var posx = $('#elemPosX').text();
    var posy = $('#elemPosY').text();
    var ancho = $('#elemAncho').text();
    var alto = $('#elemAlto').text();
    var contenedor = $('#mFileContenedor').text();

    if (contenedor === "") {
        contenedor = "cuerpo_pizarra";
    }

    if (posx === "-1") {
        posx = Math.floor(Math.random() * 300) + 50;
        posy = Math.floor(Math.random() * 300) + 50;
        ancho = "200";
        alto = "100";
    }


    if ($('#tipoMedia').val() === "Imagen") {
        AgregarImagen(
            "imagen_" + idNumero, "'" + archivo + "'", ancho, alto, posx, posy, 1, idNumero, idNumero, contenedor);

    } else {
        AgregarVideo(
            "video_" + idNumero, archivo, ancho, alto, posx, posy, 1, idNumero, idNumero, contenedor);
    }

    $('#elemPosX').text("-1");
    $('#elemPosY').text("-1");
    $('#elemAncho').text("-1");
    $('#elemAlto').text("-1");
    $('#mFileContenedor').text("");
    $('#modalFile').modal("hide");

}
function mostrarRespuesta(mensaje, ok) {
    $("#respuesta").removeClass('alert-success').removeClass('alert-danger').html(mensaje);
    if (ok) {
        $("#respuesta").addClass('alert-success');
    } else {
        $("#respuesta").addClass('alert-danger');
    }
}
$(document).ready(function () {

    //mostrarArchivos();
    //$("#boton_subir").on('click', function () {
    //    subirArchivos();
    //});
    $("#boton_eliminar").on('click', function () {
        var archivo = $('input[name="optradio"]:checked').val();
        if (archivo != "" && archivo != undefined) {
            archivo = $.trim(archivo);
            eliminarArchivo(archivo);
        } else if ($('#tipoMedia').val() == "Imagen") {
            alertify.warning(_msgSelectAnImage);
        } else if ($('#tipoMedia').val() == "Video") {
            alertify.warning(_msgSelectAVideo);
        }
    });

    $("#boton_seleccionar").on('click', function () {
        var archivo = $('input[name="optradio"]:checked').val();
        if ($('#modalType').val() == 'Media') {
            if (archivo != "" && archivo != undefined) {
                archivo = $.trim(archivo);
                if ($('#tipoMedia').val() == "Imagen") {
                    if ($('#modalMode').val() == 'add') {
                        seleccionarArchivos('upload_files/files/' + uid + '/images/' + archivo);
                    } else {
                        componentSelected.css({
                            "background": "url(" + 'upload_files/files/' + uid + '/images/' + archivo + ")",
                            "background-size": "100% 100%",
                            "background-repeat": "no-repeat"
                        });
                    }
                } else if ($('#tipoMedia').val() == "Video") {
                    $("video").each(function () {
                        this.pause()
                    });
                    if ($('#modalMode').val() == 'add') {
                        seleccionarArchivos('upload_files/files/' + uid + '/videos/' + archivo);
                    }else{
                        componentSelected.attr('media_url', 'upload_files/files/' + uid + '/videos/' + archivo);
                        componentSelected.get(0).children[0].src =  'upload_files/files/' + uid + '/videos/' + archivo;
                    }
                }
            } else if ($('#tipoMedia').val() == "Imagen") {
                alertify.warning(_msgSelectAnImage);
            } else if ($('#tipoMedia').val() == "Video") {
                alertify.warning(_msgSelectAVideo);
            }
        } else if ($('#modalType').val() == 'Background') {
            var bgtype = $('input[name="optBackground"]:checked').val();
            if (bgtype == 'image') {
                $("#cuerpo_pizarra").css({
                    "background": "url("+ encodeURI("upload_files/files/" + uid + "/images/" + archivo ) + ")",
                    "background-size": "100% 100%",
                    "background-repeat": "no-repeat"
                });
                $("#cuerpo_pizarra").attr({
                    'media_url': 'upload_files/files/' + uid + '/images/' + archivo,
                    'imagename': archivo
                });
                $('#backgroundType').val("imagen");
            } else if (bgtype == 'color') {
                $("#cuerpo_pizarra").css({
                    "background": $('#bgColorPizarra').colorpicker('getValue')
                });
                $("#cuerpo_pizarra").attr('media_url', "");
                $('#backgroundType').val("color");
            }
        }
        $('#modalFile').modal("hide");
    });

    $("#archivos_subidos").on("click", ".radio", function (event) {
        $("#boton_eliminar").prop('disabled', false);
        $("#boton_seleccionar").prop('disabled', false);
    });

    $("#formFile").on("submit", function (e) {
        e.preventDefault();
        var validFormat = false;
        if ($("#myFile").val() != "") {
            //Verificar el tamaño del fichero.
            var fsize = $('#myFile')[0].files[0].size;
            var maxSize = $('#maxSize').val();
            //Verificar los formatos MIME para video o imagen
            var ftype = $('#myFile')[0].files[0].type;
            if ($('#tipoMedia').val() == "Imagen") {
                $data = {tipoMedia: "Imagen", uid: uid};
                switch (ftype) {
                    case 'image/jpeg':
                    case 'image/gif':
                    case 'image/png':
                    case 'image/jpg':
                        validFormat = true;
                        break;
                    default:
                        validFormat = false;
                }
            } else if ($('#tipoMedia').val() == "Video") {
                $data = {tipoMedia: "Video", uid: uid};
                switch (ftype) {
                    case 'video/ogg':
                    case 'video/mp4':
                    case 'video/webm':
                        validFormat = true;
                        break;
                    default:
                        validFormat = false;
                }
            }
            if (validFormat == true) {
                if (fsize < maxSize) {
                    $("#myFile").upload('./upload_files/subir_archivo.php',
                        $data,
                        function (respuesta) {
                            if (respuesta == 1) {
                                alertify.success(_msgFileUploaded);
                                mostrarArchivos();
                            } else if (respuesta == 0) {
                                alertify.error(_msgErrorUploadFile);
                            } else if (respuesta == 2) {
                                alertify.error(_msgErrorInvalidFormat);
                            }
                        },
                        function (progreso, valor) {
                            //Barra de progreso.
                            $("#progress").show();
                            $("#progress-bar").css("width", valor + '%');
                            $("#progress-bar").html(valor + "%");
                        }
                    );
                } else {
                    alertify.error(_msgErrorFileMaxSize);
                }
            } else {
                alertify.error(_msgErrorInvalidFormat);
            }
            setTimeout(function () {
                $("#progress").hide();
            }, 1000)
        } else {
            if ($("#tipoMedia").val() == "Imagen") {
                alertify.warning(_msgSelectAnImageFile);
            } else if ($("#tipoMedia").val() == "Video") {
                alertify.warning(_msgSelectAVideoFile);
            }
        }
    })
});

