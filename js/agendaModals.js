function createModals() {

//********BARRA CON LOS BOTONES DE LOS COMPONENTES********//
    $("#designer").append('<div id="buttonPanel"></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 5px !important; margin-top: auto !important"><div class="dragg btnCMP" id="AddContainer">' + _btnAddContainer + '</div></div>');

    $("#buttonPanel").append('<div class="btnContainer"><div class="btn-group">\n\
    <div class="dragg btnCMP dropdown-toggle" data-toggle="dropdown">' + _btnAddText + '<span class="caret"></span>\n\</div>\n\
    <ul class="dropdown-menu" role="menu">\n\
            <li><a id="AddTitle"><span class="glyphicon glyphicon-font"></span> ' + _btnAddTitle + '</a></li>\n\
            <li><a id="AddNota"><span class="glyphicon glyphicon-align-justify"></span> ' + _btnAddParagraph + '</a></li>\n\
         </ul>\n\
    </div>');

    $("#buttonPanel").append('<div class="btnContainer"><div class="btn-group">\n\
    <div class="dragg btnCMP dropdown-toggle" data-toggle="dropdown">' + _btnAddShapes + '<span class="caret"></span>\n\</div>\n\
    <ul class="dropdown-menu" role="menu">\n\
            <li><a id="AddLineH">' + _btnAddLineH + '</a></li>\n\
            <li><a id="AddLineV">' + _btnAddLineV + '</a></li>\n\
            <li><a id="AddRectangle">' + _btnAddRectangle + '</a></li>\n\
            <li><a id="AddCircle">' + _btnAddCircle + '</a></li>\n\
         </ul>\n\
    </div>');

    $("#buttonPanel").append('<div class="btnContainer"><div class="btn-group">\n\
    <div class="dragg btnCMP dropdown-toggle" data-toggle="dropdown">' + _btnAddMedia + '<span class="caret"></span>\n\</div>\n\
    <ul class="dropdown-menu" role="menu">\n\
            <li><a id="AddImagen"> <span class="glyphicon glyphicon-picture"></span> ' + _btnAddImage + '</a></li>\n\
            <li><a id="AddVideo"> <span class="glyphicon glyphicon-facetime-video"></span> ' + _btnAddVideo + '</a></li>\n\
         </ul>\n\
    </div>');

    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _lbBringToFront + '" class="btnCMP glyphicon glyphicon-arrow-up" id="bringFront"></div></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _lbSendToBack + '" class="btnCMP glyphicon glyphicon-arrow-down" id="sendBack"></div></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _btnAddTemplate + '" class="btnCMP glyphicon glyphicon-file" id="addTemplate"></div></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _btnBackground + '" class="btnCMP glyphicon glyphicon-text-background" id="editBackground"></div></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _btnEditComponent + '" class="btnCMP glyphicon glyphicon-edit" id="editComponent"></div></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _btnRemoveComponent + '" class="btnCMP glyphicon glyphicon-trash" id="removeComponent"></div></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _btnPlayCode + '" class="btnCMP glyphicon glyphicon-play" id="playCode"></div></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _btnOpenCode + '" class="btnCMP glyphicon glyphicon-open-file" id="openCode"></div></div>');
    $("#buttonPanel").append('<div class="btnContainer" style="margin-left: 10px !important;"><div data-toggle="tooltip" data-placement="bottom" title="' + _btnSaveCode + '" class="btnCMP glyphicon glyphicon-save-file" id="saveCode"></div></div>');


    $("#designer").append('<div id="myNicPanel" style=""></div>');
    $("#designer").append('<div id="cuerpo_editar"></div>');
    $("#cuerpo_editar").append('<div id="cuerpo_pizarra" style="z-index: 1; background: #ffffff; height: 445px; width: 100%; position: relative;" class="cuerpo_pizarra" media_url=""></div>');

    $("#designer").append('<div id="cuerpo_hidden" style="display: none"></div>');
    $("#cuerpo_hidden").hide();

    $("#designer").append('<div id="elemPosX" hidden>-1</div>');
    $("#designer").append('<div id="elemPosY" hidden>-1</div>');
    $("#designer").append('<div id="elemAncho" hidden>-1</div>');
    $("#designer").append('<div id="elemAlto" hidden>-1</div>');

    //********VENTANAS MODALS PARA EDITAR LOS COMPONENTES********//

    //********Modal para las notas y los titulos*********//
    $("#designer").append('<div id="modals"></div>');
    $("#modals").append(
        '<div class="modal fade" id="modalWidgetNota" role="dialog">\n\
            <div class="modal-dialog">\n\
                <div class="modal-content modal-sm">\n\
                    <div class="modal-header">\n\
                    <button type="button" class="close" data-dismiss="modal"\n\
                                aria-hidden="true">&times;</button>\n\
                        <h4 class="modal-title">' + _lbConfigureText + '</h4>\n\
                    </div>\n\
                    <div class="modal-body">\n\
                        <div class="row" >\n\
                            <div class="col-xs-6">\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBgColor + '</label>\n\
                                        <div id="colorFondoTexto" class="input-group colores col-xs-12">\n\
                                            <input type="text" value="" class="form-control"/>\n\
                                            <span class="input-group-addon"><i></i></span>\n\
                                        </div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBorderColor + '</label>\n\
                                        <div id="colorBordeTexto" class="input-group colores col-xs-12">\n\
                                            <input type="text" value="" class="form-control"/>\n\
                                            <span class="input-group-addon"><i></i></span>\n\
                                        </div>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-xs-6">\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBorderWidthPx + '</label>\n\
                                    <input type="number" id="anchoBordeTexto" min="0" step="1" class="input-group col-xs-10"/>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBorderRadiusPx + '</label>\n\
                                    <input type="number" id="radioBordeTexto" min="0" step="1" class="input-group col-xs-10"/>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                    <div class="modal-footer">\n\
                        <button id="modalWidgetNotaAceptar" type="button" class="btn btn-success btn-sm"" data-dismiss="modal"> <span class="glyphicon glyphicon-ok"></span> ' + _btnAccept + '</button>\n\
                        <button type="button" class="btn btn-danger btn-sm"" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> ' + _btnCancel + '</button>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
        </div>');

//********Modal para los cuadrados********//
    $("#modals").append(
        '<div class="modal fade" id="modalWidgetSquare" role="dialog">\n\
            <div class="modal-dialog">\n\
                <div class="modal-content modal-sm">\n\
                    <div class="modal-header">\n\
                    <button type="button" class="close" data-dismiss="modal"\n\
                                aria-hidden="true">&times;</button>\n\
                        <h4 class="modal-title">' + _lbConfigureRectangle + '</h4>\n\
                    </div>\n\
                    <div class="modal-body">\n\
                        <div class="row">\n\
                            <div class="col-xs-6">\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBgColor + '</label>\n\
                                    <div id="colorFondoCuadrado" class="input-group colores col-xs-12">\n\
                                        <input type="text" value="" class="form-control"/>\n\
                                        <span class="input-group-addon"><i></i></span>\n\
                                    </div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBorderColor + '</label>\n\
                                    <div id="colorBordeCuadrado" class="input-group colores col-xs-12">\n\
                                        <input type="text" value="" class="form-control"/>\n\
                                        <span class="input-group-addon"><i></i></span>\n\
                                    </div>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-xs-6">\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBorderWidthPx + '</label>\n\
                                    <input type="number" id="anchoBordeCuadrado" min="0" step="1" class="input-group col-xs-10"/>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBorderRadiusPx + '</label>\n\
                                    <input type="number" id="radioBordeCuadrado" min="0" step="1" class="input-group col-xs-10"/>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                    <div class="modal-footer">\n\
                        <button id="modalWidgetSquareAceptar" type="button" class="btn btn-success btn-sm"" data-dismiss="modal"> <span class="glyphicon glyphicon-ok"></span> ' + _btnAccept + ' </button>\n\
                        <button type="button" class="btn btn-danger btn-sm"" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> ' + _btnCancel + '</button>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
</div>');

//********Modal para las lineas Horizontales********//
    $("#modals").append('<div class="modal fade" id="modalWidgetLineH" role="dialog">\n\
    <div class="modal-dialog modal-sm">\n\
        <div class="modal-content">\n\
            <div class="modal-header">\n\
            <button type="button" class="close" data-dismiss="modal"\n\
                                aria-hidden="true">&times;</button>\n\
                <h4 class="modal-title">' + _lbConfigureLine + '</h4>\n\
            </div>\n\
            <div class="modal-body">\n\
                    <div class="container-fluid">\n\
                        <div class="row">\n\
                                <form method="post" class="form-horizontal">\n\
                                    <div class="form-group">\n\
                                        <label class="control-label col-xs-5"> ' + _lbColor + '\n\
                                        </label>\n\
                                        <div id="colorLineaH" class="input-group colores col-xs-5">\n\
                                            <input type="text" value="" class="form-control"/>\n\
                                            <span class="input-group-addon"><i></i></span>\n\
                                        </div>\n\
                                    </div>\n\
                                    <div class="form-group">\n\
                                        <label class="control-label col-xs-5">' + _lbWidthPx + '\n\
                                        </label>\n\
                                        <div class="input-group col-xs-5">\n\
                                            <input type="number" id="anchoLineaH" min="0" step="1" class="form-control"/>\n\
                                        </div>\n\
                                    </div>\n\
                                </form>\n\
                        </div>\n\
                    </div>\n\
            </div>\n\
            <div class="modal-footer">\n\
                <button id="modalWidgetLineHAceptar" type="button" class="btn btn-success btn-sm"" data-dismiss="modal"> <span class="glyphicon glyphicon-ok"></span> ' + _btnAccept + '\n\
                </button>\n\
                <button type="button" class="btn btn-danger btn-sm"" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> ' + _btnCancel + '</button>\n\
            </div>\n\
        </div>\n\
    </div>\n\
</div>');

    //********Modal para las lineas Verticales********//
    $("#modals").append('<div class="modal fade" id="modalWidgetLineV" role="dialog">\n\
    <div class="modal-dialog modal-sm">\n\
        <div class="modal-content">\n\
            <div class="modal-header">\n\
            <button type="button" class="close" data-dismiss="modal"\n\
                                aria-hidden="true">&times;</button>\n\
                <h4 class="modal-title">' + _lbConfigureLine + '</h4>\n\
            </div>\n\
            <div class="modal-body">\n\
                    <div class="container-fluid">\n\
                        <div class="row">\n\
                                <form method="post" class="form-horizontal">\n\
                                    <div class="form-group">\n\
                                        <label class="control-label col-xs-5"> ' + _lbColor + '\n\
                                        </label>\n\
                                        <div id="colorLineaV" class="input-group colores col-xs-5">\n\
                                            <input type="text" value="" class="form-control"/>\n\
                                            <span class="input-group-addon"><i></i></span>\n\
                                        </div>\n\
                                    </div>\n\
                                    <div class="form-group">\n\
                                        <label class="control-label col-xs-5">' + _lbWidthPx + '\n\
                                        </label>\n\
                                        <div class="input-group col-xs-5">\n\
                                            <input type="number" id="anchoLineaV" min="0" step="1" class="form-control"/>\n\
                                        </div>\n\
                                    </div>\n\
                                </form>\n\
                        </div>\n\
                    </div>\n\
            </div>\n\
            <div class="modal-footer">\n\
                <button id="modalWidgetLineVAceptar" type="button" class="btn btn-success btn-sm" data-dismiss="modal"> <span class="glyphicon glyphicon-ok"></span> ' + _btnAccept + '\n\
                </button>\n\
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> ' + _btnCancel + '</button>\n\
            </div>\n\
        </div>\n\
    </div>\n\
</div>');

    //********Modal para los Archivos Subidos********//
    $("#modals").append(
        '<div class="modal fade" role="dialog" id="modalFile">\n\
            <div class="modal-dialog">\n\
                <div class="modal-content">\n\
                    <div class="modal-header">\n\
                        <button type="button" class="close" data-dismiss="modal"\n\
                                aria-hidden="true">&times;</button>\n\
                        <h4 id="title-File" class="modal-title"></h4>\n\
                    </div>\n\
                    <div class="modal-body">\n\
                        <div id="bgPizarra" class="row">\n\
                            <div class="col-xs-4">\n\
                                <div class="radio">\n\
                                    <label class="col-xs-12">\
                                        <input type="radio" name="optBackground" value="color" checked> ' + _lbBgColor + ' </label>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-xs-4" style="padding-top: 4px">\n\
                                <div id="bgColorPizarra" class="input-group colores">\n\
                                    <input type="text" value="" class="form-control"/>\n\
                                    <span class="input-group-addon"><i></i></span>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                        <form style="display: none;" id="myFormMedia" action="../upload_images/subir_archivo.php" method="POST" enctype="multipart/form-data">\n\
                                <input id="pizarraActual" type="hidden" name="pizarra" value="">\n\
                                <input id="tipoMedia" type="hidden" name="tipo" value="">\n\
                                <input id="mFilePosX" type="hidden" name="mFilePosX" value="-1">\n\
                                <input id="mFilePosY" type="hidden" name="mFilePosY" value="-1">\n\
                                <input id="mFileAncho" type="hidden" name="mFileAncho" value="-1">\n\
                                <input id="mFileAlto" type="hidden" name="mFileAlto" value="-1">\n\
                                <input id="mFileContenedor" type="hidden" name="mMode" value="-1">\n\
                                <!-- Este input oculto define en que modo se levanta la ventana (add, edit o background)-->\n\
                                <input id="modalMode" type="hidden" name="mMode">\n\
                                <!-- Este input oculto es el que guarda que tipo de fondo tiene el cuerpo_pizarra (imagen o color)-->\n\
                                <input id="backgroundType" type="hidden" name="mTipo" value="color">\n\
                                <!-- Este input oculto es el que define que instancia de ventana se va a mostrar (imagen o video) -->\n\
                                <input id="modalType" type="hidden" name="mTipo" value="-1">\n\
                                <!-- Este input oculto es el que define el tamaño del fichero a subir en bytes-->\n\
                                <input id="maxSize" type="hidden" value="1073741824">\n\
                                <br/>\n\
                        </form>\n\
                        <div class="row" id="images">\n\
                            <div class="col-xs-12">\n\
                                <div id="bgImage" class="radio">\n\
                                    <label><input type="radio" name="optBackground" value="image"> ' + _lbBackgroundImage + '</label>\n\
                                </div>\n\
                                <div id="archivos_subidos" class="pre-scrollable" style="overflow-y: scroll; height: 170px">\n\
                            </div>\n\
                        </div>\n\
                        </div>\n\
                        <form id="formFile" method="POST" enctype="multipart/form-data" style="margin-top: 5px">\n\
                            <div class="row">\n\
                                <div class="col-sm-9" id="myFileDiv">\n\
                                    <input type="file" id="myFile" name="myfile" >\n\
                                </div>\n\
                                <div class="col-sm-3 pull-left">\n\
                                    <p class="text-danger" style="margin-top: 5px"><strong>' + _MaxFileSize + ':</strong> 1 Gb</p>\n\
                                </div>\n\
                            </div>\n\
                            <div class="row">\n\
                                <div class="col-sm-12">\n\
                                    <div id="progress" class="progress" style="display: none">\n\
                                        <div id="progress-bar" class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width:0%"> 0%\n\
                                        </div>\n\
                                    </div>\n\
                                </div>\n\
                            </div>\n\
                        </form>\n\
                    </div>\n\
                    <div class="modal-footer">\n\
                            <div class="row">\n\
                                <div class="col-xs-12 ">\n\
                                    <button id="boton_subir" type="submit" form="formFile" class="btn btn-xs btn-info pull-left"> <span class="glyphicon glyphicon-cloud-upload"></span> ' + _btnUpload + '</button>\n\
                                    <button id="boton_seleccionar" class="btn btn-xs btn-success pull-left"> <span class="glyphicon glyphicon-ok"></span> ' + _btnSelect + ' </button>\n\
                                    <button id="boton_eliminar" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span> ' + _btnRemove + ' </button>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
        </div>');

    /**
     * Modal para los circulos
     */
    $("#modals").append(
        '<div class="modal fade" id="modalWidgetCircle" role="dialog">\n\
            <div class="modal-dialog">\n\
                <div class="modal-content modal-sm">\n\
                    <div class="modal-header">\n\
                    <button type="button" class="close" data-dismiss="modal"\n\
                                aria-hidden="true">&times;</button>\n\
                        <h4 class="modal-title">' + _lbConfigureCircle + '</h4>\n\
                    </div>\n\
                    <div class="modal-body">\n\
                        <div class="row">\n\
                            <div class="col-xs-6">\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBgColor + '</label>\n\
                                    <div id="colorFondoCirculo" class="input-group colores col-xs-12">\n\
                                        <input type="text" value="" class="form-control"/>\n\
                                        <span class="input-group-addon"><i></i></span>\n\
                                    </div>\n\
                                </div>\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBorderColor + '</label>\n\
                                    <div id="colorBordeCirculo" class="input-group colores col-xs-12">\n\
                                        <input type="text" value="" class="form-control"/>\n\
                                        <span class="input-group-addon"><i></i></span>\n\
                                    </div>\n\
                                </div>\n\
                            </div>\n\
                            <div class="col-xs-6">\n\
                                <div class="form-group">\n\
                                    <label>' + _lbBorderWidthPx + '</label>\n\
                                    <input type="number" id="anchoBordeCirculo" min="0" step="1" class="input-group col-xs-10"/>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                    <div class="modal-footer">\n\
                        <button id="modalWidgetCircleAceptar" type="button" class="btn btn-success btn-sm"" data-dismiss="modal"> <span class="glyphicon glyphicon-ok"></span> ' + _btnAccept + ' </button>\n\
                        <button type="button" class="btn btn-danger btn-sm"" data-dismiss="modal"> <span class="glyphicon glyphicon-remove"></span> ' + _btnCancel + '</button>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
        </div>');

    //********Modal para las Plantillas del Editor********//
    $("#modals").append(
        '<div class="modal fade" role="dialog" id="modalTemplates">\n\
            <div class="modal-dialog">\n\
                <div class="modal-content">\n\
                    <div class="modal-header">\n\
                        <button type="button" class="close" data-dismiss="modal"\n\
                                aria-hidden="true">&times;</button>\n\
                        <h4 id="title-File" class="modal-title"> ' + _lbSelectATemplate + ' </h4>\n\
                    </div>\n\
                    <div class="modal-body">\n\
                        <div class="row">\n\
                            <div class="col-xs-12">\n\
                                <div id="plantillas" class="pre-scrollable" style="overflow-y: scroll; height: 300px">\n\
                            </div>\n\
                        </div>\n\
                        </div>\n\
                    </div>\n\
                    <div class="modal-footer">\n\
                            <div class="row">\n\
                                <div class="col-xs-12 ">\n\
                                    <button id="boton_seleccionar_plantilla" class="btn btn-xs btn-success"> <span class="glyphicon glyphicon-ok"></span> ' + _btnSelect + ' </button>\n\
                                    <button id="boton_cancelar" class="btn btn-xs btn-danger" data-dismiss="modal" > <span class="glyphicon glyphicon-remove"></span> ' + _btnCancel + '</button>\n\
                                </div>\n\
                            </div>\n\
                        </div>\n\
                    </div>\n\
                </div>\n\
            </div>\n\
        </div>');
}
;