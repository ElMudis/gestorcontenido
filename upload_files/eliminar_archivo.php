<?php
if (isset($_POST['archivo']) && isset($_POST['tipoMedia']) && isset($_POST['uid'])) {
    $archivo = $_POST['archivo'];
    $tipoMedia = $_POST['tipoMedia'];
    $uid = $_POST['uid'];
    if ($tipoMedia == "Imagen") {
        if (file_exists("files/$uid/images/$archivo")) {
            unlink("files/$uid/images/$archivo");
            echo 1;
        } else {
            echo 0;
        }
    }else if($tipoMedia == "Video"){
        if (file_exists("files/$uid/videos/$archivo")) {
            unlink("files/$uid/videos/$archivo");
            echo 1;
        } else {
            echo 0;
        }
    }
}
?>
