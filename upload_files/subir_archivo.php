<?php
if (isset($_FILES['myfile']) && isset($_POST['tipoMedia']) && isset($_POST['uid'])) {
    $archivo = $_FILES['myfile'];
    $tipoMedia = $_POST['tipoMedia'];
    $uid = $_POST['uid'];
    $nombre = $archivo['name'];
    $FileType = $archivo['type'];
//    $UploadMaxFilesize = ini_get("upload_max_filesize");
    if ($tipoMedia == "Imagen") {
        //Verificando el MIME
        $isMIME = array_search($FileType,
            array(
                'jpeg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
                'jpg' => 'image/jpg',
            ), true);
        if ($isMIME != false) {
            $fileName = getFileName($uid, "images", $nombre);
            if (move_uploaded_file($archivo['tmp_name'], "files/$uid/images/$fileName")) {
                echo 1; // Success
            } else {
                echo 0; // Fallo la subida, error desconocido
            }

        } else {
            echo(2); //Formato MIME no valido
        }
    } else if ($tipoMedia == "Video") {
        //Verificando el MIME
        $isMIME = array_search(
            $FileType,
            array(
                'mp4' => 'video/mp4',
                'ogg' => 'video/ogg',
                'webm' => 'video/webm',
            ), true);
        if ($isMIME != false) {
            $fileName = getFileName($uid, "videos", $nombre);
            if (move_uploaded_file($archivo['tmp_name'], "files/$uid/videos/$fileName")) {
                echo 1; // Success
            } else {
                echo 0; // Fallo la subida, error desconocido
            }

        } else {
            echo(2); //Formato no valido
        }
    }
}

/**
 * Funcion para no sobrescribir los ficheros cuando se suben al servidor
 *
 * @param $uid Id del usuario logeado en el sistema
 * @param $mediaType El tipo de media que se está subiendo (imagen o video)
 * @param $fileName Nombre del fichero que se está subiendo
 */
function getFileName($uid, $mediaType, $fileName)
{
    $count = 1;
    $info = pathinfo($fileName);
    $extension = $info['extension'];
    $fileName =  basename($fileName,'.'.$extension);
    $fileNameAux = $fileName;
    while (true) {
        if (!is_file("files/$uid/$mediaType/$fileNameAux.$extension")) {
            return $fileNameAux.".".$extension;
        } else {
            $fileNameAux = $fileName."_".$count++;
        }
    }
}
?>
