<?php
$tipoMedia = $_POST['tipoMedia'];
$uid = $_POST['uid'];
$directorio_escaneado = "";
$dirSuccess = true;
if(!is_dir("files/$uid/")){
    if(!mkdir("files/$uid/images", 0777, true) || !mkdir("files/$uid/videos", 0777, true)){
        echo 0;
        $dirSuccess = false;
    }
}
if($tipoMedia == "imagen"){
    $directorio_escaneado = scandir("files/$uid/images/");
}else if($tipoMedia == "video"){
    $directorio_escaneado = scandir("files/$uid/videos/");
}
$respuesta = array();
$archivos = array();
foreach ($directorio_escaneado as $item) {
    if ($item != '.' and $item != '..') {
        $archivos[] = $item;
    }
}
$respuesta[] = $dirSuccess;
$respuesta[] = $archivos;
echo json_encode($respuesta);
?>
